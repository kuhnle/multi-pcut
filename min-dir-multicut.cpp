#ifndef DIR_MULTICUT_CPP
#define DIR_MULTICUT_CPP

#include <ilcplex/ilocplex.h>
#include <igraph.h>
#include "mygraph.cpp"
#include <map>

using namespace mygraph;

namespace mdmc {
   typedef mygraph::fEdge Pair;

   struct Args {
      vector < Pair > P;
      vector < mygraph::fEdge > S; //edges cut
      tinyGraph g;
      uint32_t T;
      bool bVerbose = false;
      unsigned nThreads = 1;
   };

   void enumBoundedMulti( vector< Pair >& Pi, vector< NodePath >& iPaths, tinyGraph& g, uint32_t T ) {
      iPaths.clear();
      for (size_t i = 0; i < Pi.size(); ++i) {
	 vector< NodePath > stPaths;
	 enumBoundedPaths( stPaths, Pi[i].x, Pi[i].y, g, T );
	 iPaths.insert( iPaths.end(), stPaths.begin(), stPaths.end() );
      }
   }

   struct cdArgs {
      uint32_t T;
   };

   class cdWorker {
   public:
      static void run(
		      vector< Dijkstra >::iterator itBegin,
		      vector< Dijkstra >::iterator itEnd,
		      cdArgs& args,
		      mutex& mtx ) {
	 while (itBegin != itEnd) {
	    itBegin->compute_h( args.T + 1 );
	    itBegin->save_h();
	    ++itBegin;
	 }
      }
   };
   
  class mdmcType {
   public:
      Args& args;
      vector< NodePath > allPaths;
      set< fEdge, fEdgeLT > edges;
      vector< fEdge > vEdges;
      vector< NodePath > Ph3Paths;
      Logger logg;
      map< fEdge, Dijkstra, fEdgeLT > mdist;
      igraph_t g;
      igraph_vector_t vWeights;
      size_t m; //Number of edges
      vector< igraph_t > H;
      vector< igraph_t > LS;

    //LP VARIABLES
    
    IloEnv env;
    IloModel model;
    IloNumVarArray var;
    IloRangeArray con;

    IloCplex cplex;
    
    IloObjective obj;
    
    map< size_t, size_t > eid2varid;
    map< size_t, size_t > var2eid;
    size_t nVars = 0;
    size_t nConstraints = 0;
    
    mdmcType( Args& arg ) : args( arg ), model(env), var(env), con(env), cplex(model) {
      obj = IloMinimize( env );
      model.add(obj);

	 igraph_warning_handler_t* iwh = &igraph_warning_handler_ignore;
	 igraph_set_warning_handler( iwh ); //Disable igraph warnings;
   
	 tinyGraph& G = args.g;
	 if (args.bVerbose)
	    logg << "Constructing igraph graph for mdmc..." << endL;

	 igraph_empty( &g, G.n, IGRAPH_DIRECTED );

	 igraph_vector_t vEdges;
	 igraph_vector_init( &vEdges, 2 * G.m );

	 vector < fEdge > allEdges;
	 allEdges.reserve( G.m );

	 igraph_vector_init( &vWeights, G.m );
   
	 size_t vecIndex = 0;
	 size_t weightIndex = 0;
	 for (size_t i = 0; i < G.n; ++i) {
	    for (size_t j = 0; j < G.adjList[i].neis.size(); ++j) {
	       fEdge tmp;
	       tmp.x = i;
	       tmp.y = G.adjList[i].neis[j].getId();
	       
	       VECTOR(vEdges)[ vecIndex ] = tmp.x;
	       ++vecIndex;
	       VECTOR(vEdges)[ vecIndex ] = tmp.y;
	       ++vecIndex;

	       allEdges.push_back( tmp );

	       VECTOR(vWeights)[ weightIndex ] = 0.0;//static_cast< igraph_integer_t >( G.adjList[i].neis[j].weight );
	       ++weightIndex;
	    }
	 }

	 igraph_add_edges( &g, &vEdges, 0 );
	 m = G.m;
	 igraph_vector_destroy( &vEdges );

	 for (size_t i = 0; i < args.P.size(); ++i) {
	    igraph_t* Hi = new igraph_t;
	    igraph_t* LSi = new igraph_t;
	    H.push_back( *Hi );
	    LS.push_back( *LSi );
	 }
      }

      ~mdmcType() {
	 igraph_vector_destroy( &vWeights );
	 igraph_destroy( &g );

      }

      double calcPathDist( igraph_vector_t* vPath ) {
	 size_t vSize = igraph_vector_size( vPath );
	 double dst = 0.0;
	 for (size_t i = 0; i < vSize; ++i) {
	    size_t eid = VECTOR( *vPath )[ i ];
	    dst += VECTOR( vWeights )[ eid ];
	 }

	 if (vSize == 0) //empty paths do not need to be broken
	    dst = 1.0;
	 
	 return dst;
      }
      
      bool recomputeDists( vector< igraph_vector_t* >& vConstraints ) {
	vConstraints.clear();
	 bool rvalue = false;
	 for (size_t i = 0; i < args.P.size(); ++i) {
	    igraph_vector_t *edgePath = new igraph_vector_t;
	    igraph_vector_init( edgePath, 0 );
	    igraph_get_shortest_path_dijkstra( &g,
					       0,
					       edgePath,
					       args.P[i].x,
					       args.P[i].y,
					       &vWeights,
					       IGRAPH_OUT );
	    
	    if (calcPathDist( edgePath ) < 0.9999) {
	       vConstraints.push_back( edgePath );
	       rvalue = true;
	    }
	 }

	 return rvalue;
      }
      
      void lan_lp( bool ip = false ) {
	 if (args.bVerbose) {
	    if (ip)
	       logg << "Solving mdmc IP by Lan's method..." << endL;
	    else
	       logg << "Solving mdmc LP by Lan's method..." << endL;
	 }
	    

	 vector< igraph_vector_t* > vConstraints;
	 bool bcont = recomputeDists( vConstraints );


	 
	 while (bcont) {
	    if (ip)
	       solve_ip( vConstraints );
	    else
	       solve_lp( vConstraints );

	    bcont = recomputeDists( vConstraints );
	 }

	 for (size_t i = 0; i < vConstraints.size(); ++i) {
	    igraph_vector_destroy( vConstraints[ i ] );
	 }   
      }

      void solve_lp(vector< igraph_vector_t* >& newConstraints ) {
	size_t newVars = 0;
	
	 for (size_t i = 0; i < newConstraints.size(); ++i) {
	    igraph_vector_t* apath = newConstraints[i];
	    size_t aSize = igraph_vector_size( apath );
	    for (size_t j = 0; j < aSize; ++j) {
	       size_t k = VECTOR( *apath )[j];
	       auto iter = eid2varid.find( k );
	       if (iter == eid2varid.end()) {
		  eid2varid[ k ] = nVars;
		  var2eid[ nVars ] = k;
		  var.add( IloNumVar( env ) );
		  obj.setLinearCoef( var[ nVars ], 1.0 );
				  
		  ++newVars;
		  ++nVars;
	       }
	    }
	 }   


	 //Add constraints corresponding to each path
	 if (args.bVerbose)
	    logg << "LP has " << nVars << " variables and " << nConstraints << "constraints." << endL;

	 IloRangeArray c( env );
	 unsigned numberConstraints = 0;

	 for (size_t i = 0; i < newConstraints.size(); ++i) {
	    c.add( IloRange( env, 1.0, IloInfinity ) );
	    ++nConstraints;
	 }
	 
	 for (size_t i = 0; i < newConstraints.size(); ++i) {
	    igraph_vector_t* apath = (newConstraints[i]);
	    size_t aPathSize = igraph_vector_size( apath );
	    for (size_t j = 0; j < aPathSize; ++j) {
	      size_t k = VECTOR( *apath )[ j ];
	      c[ i ].setLinearCoef( var[ eid2varid[k] ], 1.0 );
	    }
	 }

	 model.add(c);

	 cplex.setParam(IloCplex::IntParam::Threads, args.nThreads);
	 cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
	 // //Tells cplex to use wall-clock time
	 // cplex.setParam(IloCplex::ClockType, 2); 
	 // double hour = 60.0 * 60;
	 // cplex.setParam(IloCplex::TiLim, max_hours * hour );
	 if (!args.bVerbose)
	    cplex.setOut(env.getNullStream());
    
	 cplex.solve();
	 for (size_t i = 0; i < nVars; ++i) {
	    double varValue = cplex.getValue(var[i]);
	    if (varValue < 0)
	       varValue *= -1;
	    VECTOR( vWeights )[ var2eid[i] ] = varValue;
	 }
      }

      void solve_ip(vector< igraph_vector_t* >& constraints ) {
	 IloEnv env;
	 IloModel model(env);
	 IloNumVarArray var(env);
	 IloRangeArray con(env);

	 map< size_t, size_t > eid2varid;
	 map< size_t, size_t > var2eid;
	 size_t nVars = 0;
	 for (size_t i = 0; i < constraints.size(); ++i) {
	    igraph_vector_t* apath = constraints[i];
	    size_t aSize = igraph_vector_size( apath );
	    for (size_t j = 0; j < aSize; ++j) {
	       size_t k = VECTOR( *apath )[j];
	       auto iter = eid2varid.find( k );
	       if (iter == eid2varid.end()) {
		  eid2varid[ k ] = nVars;
		  var2eid[ nVars ] = k;
		  ++nVars;
	       }
	    }
	 }   
	 
	 for (size_t i = 0; i < nVars; ++i) {
	    var.add( IloNumVar( env , 0.0, 1.0, ILOINT ) );
	 }

	 //Add constraints corresponding to each path
	 IloRangeArray c(env);
	 IloObjective obj = IloMinimize( env );

	 if (args.bVerbose)
	    logg << "IP has " << nVars << " variables and " << constraints.size() << " constraints." << endL;
	 
	 for (size_t i = 0; i < constraints.size(); ++i) {
	    c.add( IloRange( env, 1.0, IloInfinity ) );
	 }

	 for (size_t i = 0; i < nVars; ++i) {
	    obj.setLinearCoef( var[ i ], 1.0 );
	 }

	 for (size_t i = 0; i < constraints.size(); ++i) {
	    igraph_vector_t* apath = (constraints[i]);
	    size_t aPathSize = igraph_vector_size( apath );
	    for (size_t j = 0; j < aPathSize; ++j) {
	       size_t k = VECTOR( *apath )[ j ];
	       c[ i ].setLinearCoef( var[ eid2varid[k] ], 1.0 );
	    }
	 }

	 model.add(obj);
	 model.add(c);

	 IloCplex cplex( model );
	 cplex.setParam(IloCplex::IntParam::Threads, args.nThreads);
	 cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
	 // //Tells cplex to use wall-clock time
	 // cplex.setParam(IloCplex::ClockType, 2); 
	 // double hour = 60.0 * 60;
	 // cplex.setParam(IloCplex::TiLim, max_hours * hour );
	 //	 if (!args.bVerbose)
	 cplex.setOut(env.getNullStream());
    
	 cplex.solve();
	 for (size_t i = 0; i < nVars; ++i) {
	    double varValue = cplex.getValue(var[i]);
	    if (varValue < 0)
	       varValue *= -1;
	    VECTOR( vWeights )[ var2eid[i] ] = varValue;
	 }
      }
      
      void opt() {
	 if (args.bVerbose)
	    logg << "Solving mdmc instance optimally..." << endL;

	 
	 lan_lp( true );

	 for (size_t i = 0; i < m; ++i) {
	    if (VECTOR( vWeights )[i] > 0.95) {
	       int from, to;
	       igraph_edge( &g, i, &from, &to );
	       fEdge tmp;
	       tmp.x = from;
	       tmp.y = to;
	       args.S.push_back( tmp );
	    }
	 }
      }
      
      void gupta() {
	 if (args.bVerbose) {
	    logg << "Beginning min. directed multi-cut (Gupta)..." << endL;
	    logg << "Constructing multi-cut LP..." << endL;
	 //	 phaseOne();
	 }
	 
	 lan_lp();

	 for (size_t i = 0; i < m; ++i) {
	    if ( VECTOR( vWeights )[i] > 1.0 / sqrt( args.g.n ) ) {
	       int from, to;
	       igraph_edge( &g, i, &from, &to );
	       fEdge tmp;
	       tmp.x = from;
	       tmp.y = to;
	       args.S.push_back( tmp );
	    }
	 }
	 if (args.bVerbose)
	    logg << "LP finished, proceeding with second phase of Gupta's alg..." << endL;
	 phaseTwo();
      }

      void constructH( size_t ii ) {
	 igraph_t& Hi = H[ ii ];
	 Pair& p = args.P[ii];
	 node_id s = p.x;
	 node_id t = p.y;
	 //Remove s to see which vertices can reach
	 //t by a simple path, not passing through s
	 igraph_vector_t hWeights;
	 igraph_vector_init( &hWeights, m );
	 //To do this, first set edge weights
	 //of edges incident with s to one
	 igraph_vector_t eids;
	 igraph_vector_init( &eids, 0 );
	 igraph_incident( &g, &eids, s, IGRAPH_ALL );
	 size_t eidSize = igraph_vector_size( &eids );
	 for (size_t i = 0; i < eidSize; ++i) {
	    VECTOR( hWeights )[ (unsigned) VECTOR( eids )[ i ] ] = 1.0;
	 }
	 //Now consider all vertices of in-distance 0 from t
	 igraph_matrix_t t_dist;

	 igraph_matrix_init( &t_dist, 1, 1 ); //resized to 1 x n?
	 igraph_shortest_paths_dijkstra( &g,
					 &t_dist,
					 igraph_vss_1( t ),
					 igraph_vss_all(),
					 &hWeights,
					 IGRAPH_IN
					 );
	 for (size_t i = 0; i < eidSize; ++i) {
	    VECTOR( hWeights )[ (unsigned) VECTOR( eids )[ i ] ] = 0.0;
	 }

	 //NOW repeat this for s to get vertices reachable by a simple paty
	 //without passing through s
	 igraph_incident( &g, &eids, t, IGRAPH_ALL );
	 eidSize = igraph_vector_size( &eids );
	 for (size_t i = 0; i < eidSize; ++i) {
	    VECTOR( hWeights )[ (unsigned) VECTOR( eids )[ i ] ] = 1.0;
	 }
	 //Now consider all vertices of out-distance 0 from s
	 igraph_matrix_t s_dist;

	 igraph_matrix_init( &s_dist, 1, 1 ); //resized to 1 x n?
	 igraph_shortest_paths_dijkstra( &g,
					 &s_dist,
					 igraph_vss_1( s ),
					 igraph_vss_all(),
					 &hWeights,
					 IGRAPH_OUT
					 );
	 for (size_t i = 0; i < eidSize; ++i) {
	    VECTOR( hWeights )[ (unsigned) VECTOR( eids )[ i ] ] = 0.0;
	 }
	 //Now H_i should only have edges between vertices that are 0 from both s,t
	 igraph_vector_t hEdges;
	 igraph_vector_init( &hEdges, 0 );
	 for (size_t i = 0; i < m; ++i) {
	    int from, to;
	    igraph_edge( &g, i, &from, &to );
	    //Do from, to satisfy the requirement?
	    if ( MATRIX( s_dist, 0, from ) == 0.0 ) {
	       if ( MATRIX( t_dist, 0, to ) == 0.0 ) {
		  //Yes it does, let's add this edge to Hi
		  igraph_vector_push_back( &hEdges, from );
		  igraph_vector_push_back( &hEdges, to );
	       }
	    }
	 }

	 //Add all valid edges to H_i
	 igraph_empty( &Hi, args.g.n, IGRAPH_DIRECTED );
	 igraph_add_edges( &Hi, &hEdges, 0 );
	 igraph_vector_destroy( &hEdges );
	 igraph_vector_destroy( &eids );
	 igraph_matrix_destroy( &t_dist );
	 igraph_matrix_destroy( &s_dist );
      }

      bool LS_valid( igraph_matrix_t& s_dist, double min, double max, node_id v ) {
	 if ( MATRIX( s_dist, 0, v ) >= min ) {
	    if (MATRIX( s_dist, 0, v ) <= max ) {
	       return true;
	    }
	 }

	 return false;
      }
      
      void constructLevelSet( double min, double max, size_t ii ) {
	 Pair& p = args.P[ii];
	 igraph_t& Hi = H[ ii ];
	 igraph_t& LSi = LS[ ii ];
	 
	 node_id s = p.x;
	 
	 //Need to compute LP distances to all nodes from s in g
	 igraph_matrix_t s_dist;

	 igraph_matrix_init( &s_dist, 1, 1 ); //resized to 1 x n?
	 igraph_shortest_paths_dijkstra( &g,
					 &s_dist,
					 igraph_vss_1( s ),
					 igraph_vss_all(),
					 &vWeights,
					 IGRAPH_OUT
					 );
	 //Vertices n, n + 1 := the source, target vertices for the min, cut
	 igraph_empty( &LSi, args.g.n + 2, IGRAPH_DIRECTED );

	 igraph_vector_t LSedges;
	 igraph_vector_init( &LSedges, 0 );
	 size_t mm = igraph_ecount( &Hi );
	 for (size_t i = 0; i < mm; ++i) {
	    int from, to;
	    igraph_edge( &Hi, i, &from, &to );
	    //igraph_get_eid( &Hi, &eid, from, to, true, false );
	    //if (eid != -1) {
	    //This edge exists in Hi
	    //See if it falls within the level set.
	    if ( LS_valid( s_dist, min, max, from ) ) {
	       if ( LS_valid( s_dist, min, max, to ) ) {
		  igraph_vector_push_back( &LSedges, from );
		  igraph_vector_push_back( &LSedges, to );
	       } else {
		  igraph_vector_push_back( &LSedges, from );
		  igraph_vector_push_back( &LSedges, args.g.n + 1 );
	       }
	    } else {
	       if ( LS_valid( s_dist, min, max, to ) ) {
		  igraph_vector_push_back( &LSedges, args.g.n );
		  igraph_vector_push_back( &LSedges, to );
	       }
	    }
	       //	    }
	 }

	 igraph_add_edges( &LSi, &LSedges, 0 );
	 igraph_simplify( &LSi, true, true, 0 );
      }

      void min_cut( size_t ii ) {
	 igraph_t& LSi = LS[ ii ];
	 size_t mm = igraph_ecount( &LSi );
	 igraph_vector_t mcWeights;
	 igraph_vector_init( &mcWeights, mm );

	 for (size_t i = 0; i < mm; ++i) {
	    int from,to, eid;
	    igraph_edge( &LSi, i, &from, &to );
	    if ( static_cast< unsigned >(from) < args.g.n) {
	       if ( static_cast< unsigned >(to) < args.g.n) {
		  igraph_get_eid( &g, &eid, from, to, true, false );
		  VECTOR( mcWeights )[ i ] = VECTOR( vWeights )[ eid ];
	       } else {
		  VECTOR( mcWeights )[ i ] = m + 1;
	       }
	    } else {
	       VECTOR( mcWeights )[ i ] = m + 1;
	    }
	 }

	 // //SET INFINITE WEIGHTS
	 // igraph_vector_t eids;
	 // igraph_vector_init( &eids, 0 );
	 // igraph_incident( &LSi, &eids, args.g.n, IGRAPH_ALL );
	 // size_t eidSize = igraph_vector_size( &eids );
	 // for (size_t i = 0; i < eidSize; ++i) {
	 //    VECTOR( mcWeights )[ (unsigned) VECTOR( eids )[ i ] ] = m + 1;
	 // }

	 // igraph_incident( &LSi, &eids, args.g.n + 1, IGRAPH_ALL );
	 // eidSize = igraph_vector_size( &eids );
	 // for (size_t i = 0; i < eidSize; ++i) {
	 //    VECTOR( mcWeights )[ (unsigned) VECTOR( eids )[ i ] ] = m + 1;
	 // }

	 double cutVal;
	 igraph_vector_t cutIds;
	 igraph_vector_init( &cutIds, 0 );
	 if (args.bVerbose) {
	    logg << INFO;
	    logg << "Performing igraph min. cut..." << endL;
	 }
	 
	 igraph_st_mincut( &g, &cutVal, &cutIds, 0, 0, args.g.n, args.g.n+1, &mcWeights );

	 size_t SS = igraph_vector_size( &cutIds );
	 for (size_t i = 0; i < SS; ++i) {
	    int from, to;
	    size_t eid = VECTOR( cutIds )[ i ];
	    igraph_edge( &LSi, eid, &from, &to );
	    fEdge tmp;
	    tmp.x = from;
	    tmp.y = to;
	    args.S.push_back( tmp );
	 }

	 igraph_vector_destroy( &mcWeights );
	 igraph_vector_destroy( &cutIds );
      }
      
      void phaseTwo() {
	 if (args.bVerbose)
	    logg << "Constructing H_i, level sets, and computing min. cuts..." << endL;
	 for (size_t i = 0; i < args.P.size(); ++i) {
	    constructH( i );
	    constructLevelSet( 1.0 / 3.0, 2.0 / 3.0, i );
	 }

	 
	 for (size_t i = 0; i < args.P.size(); ++i) {
	    igraph_destroy( &(H[i]) );
	 }
      }

      void phaseOne() {
	 allPaths.clear();
	 vector< vector< Pair > > Pdiv (args.nThreads);
	 vector< vector< NodePath > > PathsPart( args.nThreads ); 
	 vector< Pair >::iterator itBegin, itEnd;
	 itBegin = args.P.begin();
	 thread* enum_threads = new thread[ args.nThreads ];
	 for (size_t i = 0; i < args.nThreads; ++i) {
	    if (i == args.nThreads - 1) {
	       //Give rest of P to thread i
	       itEnd = args.P.end();
	    } else {
	       itEnd = itBegin + ((args.P.size())/args.nThreads);
	    }

	    Pdiv[i].assign( itBegin, itEnd );

	    enum_threads[i] = thread( enumBoundedMulti, ref( Pdiv[i] ), ref( PathsPart[i] ), ref( args.g ), args.g.n + 1 );
	    itBegin = itEnd;
	 }

	 for (size_t i = 0; i < args.nThreads; ++i) {
	    enum_threads[i].join();
	    allPaths.insert( allPaths.end(), PathsPart[i].begin(), PathsPart[i].end() );      
	 }

	 delete [] enum_threads;
    
	 for (size_t i = 0; i < allPaths.size(); ++i) {
	    //Add edges of all valid paths to 'edges'
	    for (size_t j = 0; j < allPaths[i].nodes.size() - 1; ++j) {
	       unsigned char w = args.g.getEdgeWeight( allPaths[i].nodes[j], allPaths[i].nodes[j + 1] );
	       fEdge ee( allPaths[i].nodes[j], allPaths[i].nodes[j + 1], w );
	       pair< set< fEdge, fEdgeLT>::iterator, bool> ret = edges.insert( ee );
	       allPaths[i].edges.push_back( ret.first );
	    }
	 }

	 vEdges.assign( edges.begin(), edges.end() );
	 solve_lp();
      }

      size_t getIndex( const node_id& x, const node_id& y ) {
	 fEdge e(x,y);
	 for (size_t i = 0; i < vEdges.size(); ++i) {
	    if ( e == vEdges[i] )
	       return i;
	 }

	 return 0;
      }
  
      void solve_lp() {
	 tinyGraph& g = args.g;

	 IloEnv env;
	 IloModel model(env);
	 IloNumVarArray var(env);
	 IloRangeArray con(env);

	 for (size_t i = 0; i < vEdges.size(); ++i) {
	    var.add( IloNumVar( env ) );
	 }

	 // IloExpr edge_obj( env, 0 );
	 // for (size_t i = 0; i < vEdges.size(); ++i) {
	 //   edge_obj += var[i];
	 // }

	 //    model.add( IloMinimize( env, edge_obj ) );

	 //Add constraints corresponding to each path
	 IloRangeArray c(env);
	 IloObjective obj = IloMinimize( env );
    
	 for (size_t i = 0; i < allPaths.size(); ++i) {
	    c.add( IloRange( env, 1.0, IloInfinity ) );
	 }

	 for (size_t i = 0; i < vEdges.size(); ++i) {
	    obj.setLinearCoef( var[ i ], 1.0 );
	 }

	 for (size_t i = 0; i < allPaths.size(); ++i) {
	    NodePath& apath = allPaths[i];
	    for (size_t j = 0; j < apath.nodes.size() - 1; ++j) {
	       size_t k = getIndex( apath.nodes[j], apath.nodes[j + 1] );
	       c[ i ].setLinearCoef( var[ k ], 1.0 );
	    }
	 }

	 model.add(obj);
	 model.add(c);

	 IloCplex cplex( model );
	 cplex.setParam(IloCplex::IntParam::Threads, args.nThreads);
	 cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
	 // //Tells cplex to use wall-clock time
	 // cplex.setParam(IloCplex::ClockType, 2); 
	 // double hour = 60.0 * 60;
	 // cplex.setParam(IloCplex::TiLim, max_hours * hour );
	 if (!args.bVerbose)
	    cplex.setOut(env.getNullStream());
    
	 cplex.solve();
	 set< fEdge, fEdgeLT >::iterator it = edges.begin();
	 for (size_t i = 0; i < vEdges.size(); ++i ) {
	    it->w = cplex.getValue(var[i]);
	    if (it->x != vEdges[i].x || it->y != vEdges[i].y) {
	       logg << ERROR << "Edges inconsistent in lp_solve()" << endL;
	       logg << INFO;
	    }

	    if (args.bVerbose) {
	       logg << "(" << it->x << "," << it->y << "): " << it->w << endL;
	    }

	    if (it->w > 1.0 / sqrt( args.g.n )) {
	       args.S.push_back( *it );
	       args.g.remove_edge_half( it->x, it->y );
	    }
	    ++it;
	 }

      }

      void solve_ip() {
	 tinyGraph& g = args.g;

	 IloEnv env;
	 IloModel model(env);
	 IloNumVarArray var(env);
	 IloRangeArray con(env);

	 for (size_t i = 0; i < vEdges.size(); ++i) {
	    var.add( IloNumVar( env, 0.0, 1.0, ILOINT ) );
	 }

	 // IloExpr edge_obj( env, 0 );
	 // for (size_t i = 0; i < vEdges.size(); ++i) {
	 //   edge_obj += var[i];
	 // }

	 //    model.add( IloMinimize( env, edge_obj ) );

	 //Add constraints corresponding to each path
	 IloRangeArray c(env);
	 IloObjective obj = IloMinimize( env );
    
	 for (size_t i = 0; i < allPaths.size(); ++i) {
	    c.add( IloRange( env, 1.0, IloInfinity ) );
	 }

	 for (size_t i = 0; i < vEdges.size(); ++i) {
	    obj.setLinearCoef( var[ i ], 1.0 );
	 }

	 for (size_t i = 0; i < allPaths.size(); ++i) {
	    NodePath& apath = allPaths[i];
	    for (size_t j = 0; j < apath.nodes.size() - 1; ++j) {
	       size_t k = getIndex( apath.nodes[j], apath.nodes[j + 1] );
	       c[ i ].setLinearCoef( var[ k ], 1.0 );
	    }
	 }

	 model.add(obj);
	 model.add(c);

	 IloCplex cplex( model );
	 cplex.setParam(IloCplex::IntParam::Threads, args.nThreads);
	 cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
	 // //Tells cplex to use wall-clock time
	 // cplex.setParam(IloCplex::ClockType, 2); 
	 // double hour = 60.0 * 60;
	 // cplex.setParam(IloCplex::TiLim, max_hours * hour );
	 if (!args.bVerbose)
	    cplex.setOut(env.getNullStream());
    
	 cplex.solve();
	 set< fEdge, fEdgeLT >::iterator it = edges.begin();
	 for (size_t i = 0; i < vEdges.size(); ++i ) {
	    it->w = cplex.getValue(var[i]);
	    if (it->x != vEdges[i].x || it->y != vEdges[i].y) {
	       logg << ERROR << "Edges inconsistent in lp_solve()" << endL;
	       logg << INFO;
	    }

	    if (args.bVerbose) {
	       logg << "(" << it->x << "," << it->y << "): " << it->w << endL;
	    }

	    if (it->w >= 0.95) {
	       args.S.push_back( *it );
	       args.g.remove_edge_half( it->x, it->y );
	    }
	    ++it;
	 }

      }
   };

}


#endif
