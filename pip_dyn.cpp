#include "mygraph.cpp"
#include "algs.cpp"
#include "proc_size.cpp"

#include <iostream>
#include <string>
#include <unistd.h>
#include <chrono>

using namespace mygraph;
using namespace std;

void print_help() {
   
  cout << "Options: " << endl;
  cout << "-G <graph filename in binary format>" << endl
       << "-p <target pair filename>" << endl
       << "-T <threshold value>" << endl
       << "-x <max number of threads (default 1)>" << endl
       << "-A [Add <N> edges]" << endl
       << "-R [Remove <N> edges]" << endl
       << "-N <number of edges> (default: 10^6)" << endl
       << "-o <outputFileName>" << endl;
  
}

void parseArgs( int argc, char** argv, Args& arg, bool& bAdd, size_t& N, bool& bOpt, bool& bTest ) {
  int c;
  extern char *optarg;

  if (argc == 1) {
    print_help();
    exit( 2 );
  }

  string sarg;
  
  while ((c = getopt( argc, argv, ":G:DFOT:p:vx:PSEBMN:o:LAR") ) != -1) {
    switch(c) {
    case 'F':
       bTest = true;
       break;
    case 'o':
       sarg.assign( optarg );
       arg.outputFileName = sarg;
       break;
    case 'N':
       sarg.assign( optarg );
       N = stoi( sarg );
       break;
    case 'A':
       bAdd = true;
       break;
    case 'R':
       bAdd = false;
       break;
    case 'O':
       bOpt = true;
       break;
    case 'x':
      sarg.assign( optarg );
      arg.nThreads = stoi( sarg );
      break;
    case 'v':
      arg.bVerbose = true;
      break;
    case 'G':
      //graph specification
      arg.graphFileName.assign( optarg );
      break;
    case 'p':
      //target specification
      arg.pairFileName.assign( optarg );
      break;
    case 'T':
      sarg.assign( optarg );
      arg.T = stoi( sarg );
      break;
    case '?':
      print_help();
      exit( 2 );
      break;
    }
  }
}

void readGraph( Args& args ) {
   args.logg( INFO, "Reading graph from file: " + args.graphFileName + "..." );
   args.g.read_bin( args.graphFileName );
   args.logg( INFO, "Reading pairs from file: " + args.pairFileName + "..." );
   readPairs( args );
   args.logg( INFO, "Input finished.");
   args.logg << "n = " << args.g.n << ", m = " << args.g.m / 2<< endL;
}

void runStatic( Args& args ) {
  clock_t t_start = clock();
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  switch (args.alg) {
  case DARP:
    {
      args.logg(INFO, "Starting DARP..." );
      Darp darp( args );
      darp.run();
    }
    break;

  case OPT:
    {
      args.logg(INFO, "Starting OPT..." );
      Opt opt( args );
      opt.run();
    }
    break;

  case PARL:
     {
	args.logg(INFO, "Starting PARL..." );
	Parl parl( args );
	parl.run();
     }
     break;
  case PRIMALDUAL:
    {
      args.logg(INFO, "Starting PRIMALDUAL..." );
      Darp pd( args );
      pd.run();
    }
    break;
    
  case GSAMP:
    {
      args.logg(INFO, "Starting GSAMP..." );
      Gsamp gsamp( args );
      gsamp.run();
    }
    break;

  case GEN:
    {
      args.logg(INFO, "Starting GEN..." );
      Gen gen( args );
      gen.run();
    }
    break;

  case BNB:
    {
      args.logg(INFO, "Starting BNB..." );
      BnB bnb( args );
      bnb.run();
    }
    break;

  case MIA:
    {
       //args.logg(INFO, "Starting ..." );
      Mia mia( args );
      mia.run();
    }
    break;
  case MIAE:
    {
       //args.logg(INFO, "Starting ..." );
      Mia mia( args );
      mia.run();
    }
    break;
   
  }

  args.tElapsed = elapsedTime( t_start );
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  size_t WallTimeMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
  args.wallTime = WallTimeMillis / 1000.0; 
}

void removeDuplicates( vector< fEdge >& v ) {
   vector < fEdge > removedEdges;
   for (size_t i = 0; i < v.size(); ++i) {
      if (!ctnsEdge( removedEdges, v[i] )) {
	 removedEdges.push_back( v[i] );
      }
   }

   swap( v, removedEdges );
}

void removeS( Args& args ) {
  vector < fEdge > removedEdges;
  for (size_t i = 0; i < args.S.size(); ++i) {
    if (!ctnsEdge( removedEdges, args.S[i] )) {
      removedEdges.push_back( args.S[i] );
      if ( args.g.remove_edge( args.S[i].x , args.S[i].y ) == 0) {
	args.logg << ERROR << "S contains an edge not present in the graph:";
	args.logg << args.S[i].x << " " << args.S[i].y << endL;
	args.logg << INFO;
      }
    } else {
      args.logg << ERROR << "S contains duplicate edge:";
      args.logg << args.S[i].x << " " << args.S[i].y << endL;
      args.logg << INFO;
    }
  }
}


void testFeasibility( Args& args ) {
  args.logg << "Starting feasibility test..." << endL;

  args.bFeasible = true;
  for (size_t i = 0; i < args.nPairs; ++i ) {
      Dijkstra dij( args.P[i].x , args.P[i].y, args.g );
      uint32_t dst = dij.compute_dij();
      if (args.bVerbose)
	cout << "d( " << args.P[i].x << " , " << args.P[i].y << " ): " << dst << ", T: " << args.T << endl;
      if (dst <= args.T) {
	args.bFeasible = false;
	break;
      }
   }

   if (!args.bFeasible) {
     args.logg << ERROR << "Solution is infeasible." << endL << INFO;
   } else {
     args.logg << INFO << "Solution has been verified to be feasible." << endL;
   }
}




void outputResults( Args& args ) {
  args.logg << INFO;
  args.logg << "Size W: " << args.W.size() << endL;
  args.logg << "Size S: " << args.S.size() << endL;
  if (args.bVerbose) {
    print_vector( args.S, cout );
  }
  args.logg << "Elapsed CPU time(s): " << args.tElapsed << endL;
  args.logg << "Elapsed WALL time(s): " << args.wallTime << endL;

  if (!args.outputFileName.empty()) {
     args.maxMem = getPeakRSS() / (1024.0 * 1024.0);
     ofstream ofile ( args.outputFileName.c_str(), ofstream::out | ofstream::app );
     ofile << args.graphFileName << ' ' << args.alg << ' ' << args.T << ' ' << args.nPairs << ' '
	   << args.S.size() << ' ' << args.tElapsed << ' ' << args.wallTime << ' ' << args.maxMem << ' ' << args.miaCuts << ' ' << args.nThreads << endl;
     ofile.close();
  }
}


size_t dynamicRun( Args& args, bool bAdd, size_t N ) {
   args.logg << "Beginning dynamic run..." << endL;
   args.logg << "Solving initial instance..." << endL;
   Darp darp( args );
   darp.run();
   args.logg << "Initial solution:" << endL;
   args.logg << "Size S: " << args.S.size() << endL;
   args.logg << "Size W: " << args.W.size() << endL;
   args.logg << "Edges before dynamic start: " << args.g.m / 2 << endL;
   size_t elapsedMillis = 0;
   if (bAdd) {
      darp.addition_prep();
      uniform_int_distribution< node_id > udist(0, args.g.n - 1 );
      size_t eAdded = 0;
      size_t displayProgress = 0.01* N + 1;
      while (eAdded < N) {
	 if ( eAdded % displayProgress == 0 ) {
	    cout << "\r                     \r" << eAdded; cout.flush();
	 }
	 
	 //Try to add an edge
	 node_id from = udist( gen );
	 node_id to = udist( gen );
	 std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	 if (darp.dynamicAddEdge( from, to )) {
	    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	    size_t WallTimeMillis = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
	    elapsedMillis += WallTimeMillis;
	    ++eAdded;

	    if (args.bVerbose) {
	       args.logg << "Added edge " << eAdded << endL;
	       args.logg << from << " " << to << endL;
	    }
	 }
      }
      
      args.logg << "Adding " << N << " edges took " << elapsedMillis / 1000.0 / 1000.0 << " seconds." << endL;
      args.logg << "Average microseconds: " << elapsedMillis / static_cast<double>( N ) << endL;
      args.logg << "Size S: " << args.S.size() << endL;
      args.logg << "Size W: " << args.W.size() << endL;

      removeDuplicates( args.S );
      removeDuplicates( args.W );

      args.logg << "Size S: " << args.S.size() << endL;
      args.logg << "Size W: " << args.W.size() << endL;
      
   } else {
      double probS = args.S.size() / static_cast<double>( args.g.m ) * 2.0;
      uniform_real_distribution< double > zo(0.0,1.0);
      uniform_int_distribution< size_t > nDist( 0, args.g.n - 1 );
      
      size_t eRemed = 0;
      while (eRemed < N) {
	 //Remove an edge
	 double fromS = zo( gen );
	 if (fromS < probS) {
	    //select an edge in S to remove
	    uniform_int_distribution< size_t > Sdist( 0, args.S.size() - 1);
	    size_t rIndex = Sdist( gen );
	    
	    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	    darp.dynamicRemoveEdge( args.S[fromS].x, args.S[fromS].y );
	    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	    size_t WallTimeMillis = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
	    elapsedMillis += WallTimeMillis;
	    
	 } else {
	    //Select a node randomly
	    size_t nIndex;
	    do {
	       nIndex = nDist( gen );
	    } while ( args.g.adjList[ nIndex ].neis.size() == 0 );

	    //Select an edge of this node randomly
	    uniform_int_distribution< size_t > eDist( 0, args.g.adjList[ nIndex ].neis.size() - 1 );
	    size_t eIndex = eDist( gen );
	    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
	    darp.dynamicRemoveEdge( nIndex, args.g.adjList[ nIndex ].neis[ eIndex ].getId() );
	    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	    size_t WallTimeMillis = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
	    elapsedMillis += WallTimeMillis;
	 }

	 ++eRemed;
      }

      args.logg << "Removing " << N << " edges took " << elapsedMillis / 1000.0 / 1000.0 << " seconds." << endL;
      args.logg << "Average microseconds: " << elapsedMillis / static_cast<double>( N ) << endL;
      args.logg << "Size S: " << args.S.size() << endL;
      args.logg << "Size W: " << args.W.size() << endL;

      removeDuplicates( args.S );
      removeDuplicates( args.W );

      args.logg << "Size S: " << args.S.size() << endL;
      args.logg << "Size W: " << args.W.size() << endL;
      
   }

   return elapsedMillis;
}

int main(int argc, char** argv) {
  Args args;
  bool bAdd = true;
  size_t N = 1000000;
  bool bOpt = false;
  bool bTest = false;
  parseArgs( argc, argv, args, bAdd, N, bOpt, bTest );
  readGraph( args );
  size_t mInit = args.g.m / 2;
  args.alg = DARP;
  //runStatic( args );

  size_t dMillis = dynamicRun( args, bAdd, N );

  args.logg << "Modified graph has n = " << args.g.n << ", m = " << args.g.m / 2<< endL;
  args.logg << "New solution has size: " << args.S.size() << endL;;  

  size_t dS = args.S.size();
  size_t dW = args.W.size();

  if (bTest) {
     bool boldVerbose = args.bVerbose;
     args.bVerbose = true;
     testFeasibility( args );
     args.bVerbose = boldVerbose;
  }
  
  args.logg << "Now solving statically on final graph..." << endL;
  for (size_t i = 0; i < args.S.size(); ++i) {
     args.g.add_edge( args.S[i].x, args.S[i].y );
  }

  //  args.g.write_bin( "tmp.bin" );
  //  args.graphFileName = "tmp.bin";
  
  args.S.clear();
  args.W.clear();

  //readGraph( args );
    
  std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
  
  Darp darp2( args );
  darp2.run();

  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

  size_t sMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

  size_t sS = args.S.size();
  size_t sW = args.W.size();
  
  args.logg << "Final static results:" << endL;
  args.logg << "Size S: " << args.S.size() << endL;
  args.logg << "Size W: " << args.W.size() << endL;

  if (bOpt ) {
     args.logg << "Now solving optimally on final graph..." << endL;
     for (size_t i = 0; i < args.S.size(); ++i) {
	args.g.add_edge( args.S[i].x, args.S[i].y );
     }

     args.S.clear();
     args.W.clear();

     Opt opt(args);
     opt.run();

     args.logg << "Opt results:" << endL;
     args.logg << "Size S: " << args.S.size() << endL;
     args.logg << "Size W: " << args.W.size() << endL;
  }
  
  if (!args.outputFileName.empty()) {
     ofstream ofile ( args.outputFileName.c_str(), ofstream::out | ofstream::app );
     ofile << args.graphFileName << ' ';

     if (bAdd)
	ofile << "dynAdd ";
     else
	ofile << "dynRem ";
     
     ofile << mInit << ' '
	   << N << ' ' << dS << ' ' << dW << ' ' << dMillis << ' '
	   << dMillis / static_cast<double>(N) <<  ' ' << sS << ' ' << sW << ' '
	   << sMillis << ' ';
     if (bOpt) {
	ofile << args.S.size();
     }
     
     ofile << endl;
     ofile.close();
  }

  return 0;
}
