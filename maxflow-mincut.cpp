#ifndef MAXFLOW_MINCUT_CPP
#define MAXFLOW_MINCUT_CPP

#include <igraph.h>
#include "mygraph.cpp"
#include <map>

using namespace mygraph;

void mincut( mygraph::tinyGraph& G, //directed, weighted graph
	     mygraph::node_id s,
	     mygraph::node_id t,
	     vector< fEdge >& cutEdges )
{
   Logger logg;
   logg << "Constructing igraph graph..." << endL;
   igraph_t g;
   igraph_empty( &g, G.n, IGRAPH_DIRECTED );

   igraph_vector_t vEdges;
   igraph_vector_init( &vEdges, 2 * G.m );

   vector < fEdge > allEdges;
   allEdges.reserve( G.m );
   igraph_vector_t vWeights;
   igraph_vector_init( &vWeights, G.m );
   
   size_t vecIndex = 0;
   size_t weightIndex = 0;
   for (size_t i = 0; i < G.n; ++i) {
      for (size_t j = 0; j < G.adjList[i].neis.size(); ++j) {
	 fEdge tmp;
	 tmp.x = i;
	 tmp.y = G.adjList[i].neis[j].getId();

	 VECTOR(vEdges)[ vecIndex ] = tmp.x;
	 ++vecIndex;
	 VECTOR(vEdges)[ vecIndex ] = tmp.y;
	 ++vecIndex;

	 allEdges.push_back( tmp );

	 VECTOR(vWeights)[ weightIndex ] = static_cast< igraph_integer_t >( G.adjList[i].neis[j].weight );
	 ++weightIndex;
      }
   }

   igraph_add_edges( &g, &vEdges, 0 );

   //This code checked that eids are in order of the inserted edges
   //Indeed, they are
   // size_t eIndex = 0;
   // for (size_t i = 0; i < igraph_vector_size( &vEdges ); i+=2) {
   //    igraph_integer_t eid;
   //    igraph_get_eid( &g, &eid, VECTOR(vEdges)[ i ], VECTOR(vEdges)[ i+1 ], true, false );
   //    if ( static_cast<size_t>( eid ) != eIndex ) {
   // 	 logg << ERROR;
   // 	 logg << "Edges have changed order" << endL;
   //    }

   //    ++eIndex;
   // }

   double cutVal;
   igraph_vector_t cutIds;
   igraph_vector_init( &cutIds, 0 );
   logg << INFO;
   logg << "Performing igraph min. cut..." << endL;
   igraph_st_mincut( &g, &cutVal, &cutIds, 0, 0, s, t, &vWeights );

   cutEdges.clear();

   size_t vSize = igraph_vector_size( &cutIds );
   cutEdges.reserve( vSize );
   for (size_t i = 0; i < vSize; ++i) {
      cutEdges.push_back( allEdges[ VECTOR( cutIds )[i] ] );
   }
   
   igraph_vector_destroy( &cutIds );
   igraph_vector_destroy( &vWeights );
   igraph_vector_destroy( &vEdges );
   igraph_destroy( &g );

   logg << "done" << endL;
}



#endif
