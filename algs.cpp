#ifndef ALGS_CPP
#define ALGS_CPP

#include "mygraph.cpp"
#include "min-dir-multicut.cpp"
#include <ilcplex/ilocplex.h>
#include <set>
#include <map>
#include <string>
#include <vector>
#include <fstream>

using namespace std;
using namespace mygraph;

enum Algs {DARP=0, OPT, GSAMP, GEN, BNB, MIA, MIAE, PRIMALDUAL,PARL};

typedef fEdge Pair;

struct Args {
   Algs alg;
   string graphFileName;
   string pairFileName;
   string outputFileName = "";
   size_t nPairs;
   vector < Pair > P;
   tinyGraph g;
   uint32_t T;
   vector< fEdge > S;
   vector< fEdge > W;
   Logger logg;
   double tElapsed = 0.0;
   double wallTime = 0.0;
   bool bVerbose = false;
   bool bFeasible;
   unsigned nThreads = 1;
   bool bMiaOpt = false;
   double maxMem = 0.0;
   size_t miaCuts = 0;
};

bool ctnsEdge( vector< fEdge >& v, fEdge& e ) {
   for (size_t i = 0; i < v.size(); ++i) {
      if ( e == v[i] )
	 return true;
   }

   return false;
}

void rmEdge( vector< fEdge >& v, fEdge& e ) {
   for (auto it = v.begin(); it != v.end(); ++it) {
      if ((*it)==e) {
	 v.erase( it );
	 break;
      }
   }
}

void removeEdgeSW( Args& args, fEdge& e ) {
   for (auto it = args.S.begin(); it != args.S.end(); ++it) {
      if ((*it) == e) {
	 args.S.erase( it );
	 break;
      }
   }


   for (auto it = args.W.begin(); it != args.W.end(); ++it) {
      if ((*it) == e) {
	 args.W.erase( it );
	 break;
      }
   }
}

void removePruned( Args& args ) {
   vector< fEdge > S;
   for (size_t i = 0; i < args.S.size(); ++i) {
      if ( !ctnsEdge( args.W, args.S[i] ) ) {
	 S.push_back( args.S[i] );
      }
   }
   args.S.swap( S );
}

void enumBoundedMulti( vector< Pair >& Pi, vector< NodePath >& iPaths, tinyGraph& g, uint32_t T ) {
   iPaths.clear();
   for (size_t i = 0; i < Pi.size(); ++i) {
      vector< NodePath > stPaths;
      enumBoundedPaths( stPaths, Pi[i].x, Pi[i].y, g, T );
      iPaths.insert( iPaths.end(), stPaths.begin(), stPaths.end() );
   }
}

/*
 * Reads target pairs from file
 * args.pairFileName
 * Format is number of pairs, followed
 * by all of the pairs, plain text
 *
 */
void readPairs( Args& args ) {
   args.P.clear();
   if (args.pairFileName == "") {
      cerr << "No filename given for target pairs...\n";
      exit( 2 );
   }
   
   ifstream ifile( args.pairFileName.c_str() );
   size_t nPairs;
   ifile >> nPairs;
   Pair tmp;
   for (size_t i = 0; i < nPairs; ++i) {
      ifile >> tmp.x;
      ifile >> tmp.y;
      args.P.push_back( tmp );
   }

   args.nPairs = nPairs;
   ifile.close();
}

struct dvArgs {
   bool& bFound;
   vector< Dijkstra >::iterator itViol; //Iterator to a dijkstra object violating feasibility
   uint32_t T;
   bool update_h;
   bool useDijk;
  
   dvArgs( bool& bf, bool update_h_in = true, bool useDijk_in = false ):
      bFound( bf ), update_h( update_h_in ), useDijk( useDijk_in )  {
   }
  
};

struct dvWorker{
   static void run(
		   vector< Dijkstra >::iterator itBegin,
		   vector< Dijkstra >::iterator itEnd,
		   dvArgs& args,
		   mutex& mtx
		   ) {
      uint32_t tmpDst;
      while (itBegin != itEnd) {
	 if (args.bFound) //Check to see if we are already done
	    return;
	 if (!args.useDijk) {
	    if ( (itBegin->t_elapsed > itBegin->t_dij) && args.update_h) {
	       tmpDst = itBegin->compute_h( args.T + 1 );
	       tmpDst = itBegin->update_Astar();
	    } else {
	       tmpDst = itBegin->update_Astar();
	    }
	 } else {
	    //
	    tmpDst = itBegin->compute_dij();
	 }
      
	 if (tmpDst <= args.T) {
	    mtx.lock();
	    args.bFound = true;
	    args.itViol = itBegin;
	    mtx.unlock();
	    return;
	 }
	 ++itBegin;
      }

      return;
   }
};

struct cdArgs {
   uint32_t T;
};

class cdWorker {
public:
   static void run(
		   vector< Dijkstra >::iterator itBegin,
		   vector< Dijkstra >::iterator itEnd,
		   cdArgs& args,
		   mutex& mtx ) {
      while (itBegin != itEnd) {
	 itBegin->compute_h( args.T + 1 );
	 itBegin->save_h();
	 ++itBegin;
      }
   }
};

struct cdArgs2 {
   uint32_t T;
};

class cdWorker2 {
public:
   static void run(
		   vector< Dijkstra >::iterator itBegin,
		   vector< Dijkstra >::iterator itEnd,
		   cdArgs2& args,
		   mutex& mtx ) {
      while (itBegin != itEnd) {
	 itBegin->compute_dij();
	 ++itBegin;
      }
   }
};


typedef vector< node_id > TagPath; 

class Darp {
public:
   const uint32_t undefined = ~( static_cast< uint32_t >(0) );
   Args& args;
   vector< Dijkstra > dij;
   map< fEdge, TagPath, fEdgeLT > h;
   vector< fEdge > toPrune; // P
   
   Darp( Args& aargs ) : args( aargs ) 
   {
     args.logg( INFO, "Creating Dijkstra objects..." );
      for (size_t i = 0; i < args.nPairs; ++i) {
	 Dijkstra d1( args.P[i].x, args.P[i].y, args.g );
	 dij.push_back( d1 );
      }

      args.logg( INFO, "Initializing Dijkstra objects..." );
      cdArgs cdargs;
      cdargs.T = args.T;
      cdWorker cdworker;
      parallelWork( dij,
		    cdargs,
		    cdworker,
		    args.nThreads );
   }

   /*
    * Returns iterator to Dijkstra object
    * of first pair violating d(x,y) > T,
    * Returns dij.end() if no violations
    */
   vector<Dijkstra>::iterator detectViol( bool update_h = true, bool useDijk = false ) {
      bool bFound = false;
      dvArgs dvargs(bFound, update_h, useDijk );
      dvargs.T = args.T;
      dvWorker dvworker;
      parallelWork( dij,
		    dvargs,
		    dvworker,
		    args.nThreads );

      if (dvargs.bFound)
	 return dvargs.itViol;

      return dij.end();
   }

   void run() {
      args.logg(INFO, "Beginning removal phase..." );
      augment_base(); //no unpruning logic in this version of augment
      
      if (args.alg == PRIMALDUAL) {
	 return;
      }

      prune_base(); //using Astar to speed up Dijkstra calcs. prune() does not use this.
   }

   /* Removes shortest path between violating pair
    * as computed by update_Astar
    * from graph and adds it
    * to the solution args.S
    * 
    */
   void rmPath( vector<Dijkstra>::iterator itViol ) {
      fEdge e;
      e.x = itViol->t;
      TagPath p;
      p.push_back( e.x );
      
      do {
	 e.y = e.x;
	 e.x = itViol->prev[ e.y ];
	 e.w = static_cast< double >( args.g.remove_edge( e.x, e.y ) );
	 p.push_back( e.x );

	 if (e.w == 0) {
	    args.logg << ERROR << "Edge already removed." << endL;
	    args.logg << "Edge: " << e.x << " " << e.y << endL;
	    //print_vector( args.S );
	    args.logg << INFO;
	 }

	 args.S.push_back( e );
	 toPrune.push_back( e );
      } while ( e.x != itViol->s );

      reverse( p.begin(), p.end() );

      /*
       * Update h: S U W -> U 
       */
      for (size_t i = 0; i < p.size() - 1; ++i) {
	 fEdge f;
	 f.x = p[i];
	 f.y = p[i + 1];

	 h[ f ] = p;
      }
   }

   bool dynamicAddEdge( node_id x, node_id y, unsigned char w = 1 ) {
      fEdge e(x,y,w);
      if ( ctnsEdge( args.S, e ) )
	 return false;
      
      bool rvalue = args.g.add_edge( x, y, w );
      if (rvalue) {
	 //args.logg << INFO << args.g.m / 2 + args.S.size() << endL;

	 if (args.bVerbose)
	    args.logg << "Starting augment..." << endL;
	 
	 augment_after_addition(x, y);
	 
	 if (args.bVerbose)
	    args.logg << "Starting prune..." << endL;
	 
	 prune();

	 //args.logg << INFO << args.g.m / 2 + args.S.size() << endL;
      }

      return rvalue;
   }

   void dynamicRemoveEdge( node_id x, node_id y ) {
      fEdge e;
      e.x = x;
      e.y = y;
      auto it = h.find( e );
      if ( it == h.end() ) {
	 //This edge is not in S or W
	 //Just delete it
	 args.g.remove_edge( x, y );
	 
      } else {
	 //Solution needs to be updated
	 TagPath p = it->second;
	 for (size_t i = 0; i < p.size() - 1; ++i) {
	    fEdge f;
	    f.x = p[i];
	    f.y = p[i + 1];
	    removeEdgeSW( args, f );
	    args.g.add_edge( f.x, f.y );
	 }
	 args.g.remove_edge( x, y );
	 h.erase( it );
	 augment_removal();
	 prune();
      }
   }

   void augment_base() {
      vector<Dijkstra>::iterator itViol = dij.begin();//detectViol( true );

      while (itViol != dij.end()) {
	 itViol->update_Astar();
	 while ( itViol -> dst <= args.T ) {
	    if (args.bVerbose) {
	       args.logg << "Current pair: ("
			 << itViol->s << "," << itViol->t << "): "
			 << itViol->dst << endL;
	       args.logg << "Removing shortest path..." << endL;
	    }

	    rmPath( itViol );

	    if (args.bVerbose) {
	       args.logg << "Updating distance..." << endL;
	    }
	
	    if ( (itViol->t_elapsed > itViol->t_dij) ) {
	       itViol->compute_h( args.T + 1 );
	       itViol->update_Astar();
	    } else {
	       itViol->update_Astar();
	    }
	 }

	 ++itViol;
      }
   }

   void augment_removal() {
      vector<Dijkstra>::iterator itViol = dij.begin();//detectViol( true );

      while (itViol != dij.end()) {
	 //itViol->update_Astar();
	 itViol->compute_dij();
	 while ( itViol -> dst <= args.T ) {
	    if (args.bVerbose) {
	       args.logg << "Current pair: ("
			 << itViol->s << "," << itViol->t << "): "
			 << itViol->dst << endL;
	       args.logg << "Removing shortest path..." << endL;
	    }

	    if (!unprune( itViol )) {
	       rmPath( itViol );
	    }

	    if (args.bVerbose) {
	       args.logg << "Updating distance..." << endL;
	    }

	    itViol->compute_dij();
	    
	    // if ( (itViol->t_elapsed > itViol->t_dij) ) {
	    //    itViol->compute_h( args.T + 1 );
	    //    itViol->update_Astar();
	    // } else {
	    //    itViol->update_Astar();
	    // }
	 }

	 ++itViol;
      }
   }

   bool unprune( vector<Dijkstra>::iterator itViol ) {
      fEdge e;
      e.x = itViol->t;

      do {
	 e.y = e.x;
	 e.x = itViol->prev[ e.y ];
	 //	 e.w = static_cast< double >( args.g.remove_edge( e.x, e.y ) );

	 if ( ctnsEdge( args.S, e ) ) {
	    return true;
	 }
	 
	 if ( ctnsEdge( args.W, e ) ) {
	    //Can maintain feasibility by unpruning this edge
	    rmEdge( args.W, e );
	    args.S.push_back( e );

	    unsigned char w = args.g.remove_edge( e );
	    if (w == 0) {
	       args.logg << ERROR << "Edge already removed." << endL;
	    } else {
	       toPrune.push_back( e );
	    }
	    
	    return true;
	 }

      } while ( e.x != itViol->s );

      //Could not unprune
      return false;
   }

   void addition_prep() {
      //Update distances
      cdArgs2 cdargs;
      cdargs.T = args.T;
      cdWorker2 cdworker;
      parallelWork( dij,
		    cdargs,
		    cdworker,
		    args.nThreads );
      
      //vector<Dijkstra>::iterator itViol = dij.begin();//detectViol( true );
      //while (itViol != dij.end()) {
      //itViol->compute_dij();
      //++itViol;
      //}
   }
   
   /*
    * No Astar
    */ 
   void augment_after_addition( node_id u, node_id v ) {
      vector<Dijkstra>::iterator itViol = dij.begin();//detectViol( true );
      fEdge edgeAdded(u,v);
      while (itViol != dij.end()) {
	 //itViol->compute_dij();
	 if (!ctnsEdge( args.S, edgeAdded)) {
	    itViol->update_dij_after_addition( u, v );
	    itViol->update_dij_after_addition( v, u );
	 }
	 //bool hcomp = false;
	 while ( itViol -> dst <= args.T ) {
	    //if (!hcomp) {
	    //itViol->compute_h( args.T + 1 );
	    //itViol->update_Astar();
	    //	       hcomp = true;
	    //}
	    
	    if (args.bVerbose) {
	       args.logg << "Current pair: ("
			 << itViol->s << "," << itViol->t << "): "
			 << itViol->dst << endL;
	       args.logg << "Trying to unprune..." << endL;
	    }

	    if (!unprune( itViol ) ) {
	       if (args.bVerbose) {
		  args.logg << "Could not unprune, removing path..." << endL;
	       }
	       //Could not unprune, must remove this path
	       rmPath( itViol );
	    }

	    if (args.bVerbose) {
	       args.logg << "Updating distance..." << endL;
	    }
	
	    //itViol->update_Astar();

	    itViol->compute_dij();
	 }

	 ++itViol;
      }
   }
   
   void prune_base() {
      //Restore the original h values
      if (args.bVerbose) {
	 args.logg( INFO, "Pruning solution..." );
	 args.logg << "Restoring h values..." << endL;
      }
      
      for (size_t i = 0; i < dij.size(); ++i) {
	 dij[i].restore_h();
      }
    
      vector<Dijkstra>::iterator itViol;
      for (size_t i = 0; i < toPrune.size(); ++i) {
	 args.g.add_edge( toPrune[i].x, toPrune[i].y, static_cast<unsigned char>(toPrune[i].w) );
	 //args.logg << INFO << args.g.m / 2 << endL;
	 if (args.bVerbose) {
	    args.logg << toPrune.size() - i << " elements left. ";
	    args.logg << "Attempting to reinsert edge (" << toPrune[i].x << "," << toPrune[i].y << ")..." << endL;
	 }
	 itViol = detectViol( false, false );
	 if ( itViol != dij.end() ) {
	    //Edge required for feasibility, remove it again
	    args.g.remove_edge( toPrune[i].x, toPrune[i].y );
	    
	    if (args.bVerbose)
	       args.logg << "Feasibility violated, could not reinsert." << endL;
	 } else {
	    //Edge can be pruned
	    rmEdge( args.S, toPrune[i] );
	    args.W.push_back( toPrune[i] );
	    
	 }      
      }

      toPrune.clear();
      //removePruned( args );
   }

   /*
    * Dynamic prune. Does not use Astar
    */
   void prune() {
      vector<Dijkstra>::iterator itViol;
      for (size_t i = 0; i < toPrune.size(); ++i) {
	 args.g.add_edge( toPrune[i].x, toPrune[i].y );
	 //args.logg << INFO << args.g.m / 2 << endL;
	 if (args.bVerbose) {
	    args.logg << toPrune.size() - i << " elements left. ";
	    args.logg << "Attempting to reinsert edge (" << toPrune[i].x << "," << toPrune[i].y << ")..." << endL;
	 }
	 itViol = detectViol( false, true ); //Use dijkstra, not Astar
	 if ( itViol != dij.end() ) {
	    //Edge required for feasibility, remove it again
	    args.g.remove_edge( toPrune[i].x, toPrune[i].y );
	    
	    if (args.bVerbose)
	       args.logg << "Feasibility violated, could not reinsert." << endL;

	    if (toPrune.size() - 1 == i) {
	       //Update distances
	       cdArgs2 cdargs;
	       cdargs.T = args.T + 1;
	       cdWorker2 cdworker;
	       parallelWork( dij,
			     cdargs,
			     cdworker,
			     args.nThreads );

	    }
	    
	 } else {
	    //Edge can be pruned
	    rmEdge( args.S, toPrune[i] );
	    args.W.push_back( toPrune[i] );
	    
	 }      
      }

     
      toPrune.clear();
      //removePruned( args );
      
   }
};

class Opt {
public:
   Args& args;
   vector < NodePath > allPaths;
   vector < fEdge > vEdges;
   Opt( Args& arg ) : args( arg ) { }
  
   void run() {
      allPaths.clear();
      vector< vector< Pair > > Pdiv (args.nThreads);
      vector< vector< NodePath > > PathsPart( args.nThreads ); 
      vector< Pair >::iterator itBegin, itEnd;
      itBegin = args.P.begin();
      thread* enum_threads = new thread[ args.nThreads ];
      for (size_t i = 0; i < args.nThreads; ++i) {
	 if (i == args.nThreads - 1) {
	    //Give rest of P to thread i
	    itEnd = args.P.end();
	 } else {
	    itEnd = itBegin + ((args.P.size())/args.nThreads);
	 }

	 Pdiv[i].assign( itBegin, itEnd );

	 enum_threads[i] = thread( enumBoundedMulti, ref( Pdiv[i] ), ref( PathsPart[i] ), ref( args.g ), args.T );
	 itBegin = itEnd;
      }

      for (size_t i = 0; i < args.nThreads; ++i) {
	 enum_threads[i].join();
	 allPaths.insert( allPaths.end(), PathsPart[i].begin(), PathsPart[i].end() );      
      }

      delete [] enum_threads;
    
      set< fEdge, fEdgeLT > edges;
    
      for (size_t i = 0; i < allPaths.size(); ++i) {
	 //      allPaths[i].print( cout );
	 //Add edges of all valid paths to 'edges'
	 for (size_t j = 0; j < allPaths[i].nodes.size() - 1; ++j) {
	    fEdge ee( allPaths[i].nodes[j], allPaths[i].nodes[j + 1] );
	    edges.insert( ee );
	 }
      }

      //    cout << allPaths.size() << endl;
      //    cout << edges.size() << endl;

      vEdges.assign( edges.begin(), edges.end() );
      //    cout << vEdges.size() << endl;

      solve_ip();
   }

   size_t getIndex( const node_id& x, const node_id& y ) {
      fEdge e(x,y);
      for (size_t i = 0; i < vEdges.size(); ++i) {
	 if ( e == vEdges[i] )
	    return i;
      }

      return 0;
   }
  
   void solve_ip() {
      tinyGraph& g = args.g;

      IloEnv env;
      IloModel model(env);
      IloNumVarArray var(env);
      IloRangeArray con(env);

      for (size_t i = 0; i < vEdges.size(); ++i) {
	 var.add( IloNumVar( env, 0.0, 1.0, ILOINT ) );
      }

      // IloExpr edge_obj( env, 0 );
      // for (size_t i = 0; i < vEdges.size(); ++i) {
      //   edge_obj += var[i];
      // }

      //    model.add( IloMinimize( env, edge_obj ) );

      //Add constraints corresponding to each path
      IloRangeArray c(env);
      IloObjective obj = IloMinimize( env );
    
      for (size_t i = 0; i < allPaths.size(); ++i) {
	 c.add( IloRange( env, 1.0, IloInfinity ) );
      }

      for (size_t i = 0; i < vEdges.size(); ++i) {
	 obj.setLinearCoef( var[ i ], 1.0 );
      }

      for (size_t i = 0; i < allPaths.size(); ++i) {
	 NodePath& apath = allPaths[i];
	 for (size_t j = 0; j < apath.nodes.size() - 1; ++j) {
	    size_t k = getIndex( apath.nodes[j], apath.nodes[j + 1] );
	    c[ i ].setLinearCoef( var[ k ], 1.0 );
	 }
      }

      model.add(obj);
      model.add(c);

      IloCplex cplex( model );

      cplex.setParam(IloCplex::IntParam::Threads, args.nThreads);
      //    cplex.setParam(IloCplex::Param::Preprocessing::Linear, 0 ); //Only Linear reductions
      cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
      // //Tells cplex to use wall-clock time
      // cplex.setParam(IloCplex::ClockType, 2); 
      // double hour = 60.0 * 60;
      // cplex.setParam(IloCplex::TiLim, max_hours * hour );
      if (!args.bVerbose)
	 cplex.setOut(env.getNullStream());
    
      cplex.solve();

      for (size_t i = 0; i < vEdges.size(); ++i ) {
	 if (cplex.getValue(var[i] >= 0.95)) {
	    g.remove_edge( vEdges[i].x, vEdges[i].y );
	    args.S.push_back( vEdges[ i ] );
	 }
      }

    
   }
};

class Parl {
public:
   Args& args;
   vector< NodePath > allPaths;
   set< fEdge, fEdgeLT > edges;
   vector< fEdge > vEdges;
   vector< NodePath > Ph3Paths;
  
   Parl( Args& arg ) : args( arg ) { }
  
   void run() {
      args.logg << "Beginning phase one..." << endL;
      phaseOne();
      args.logg << "Beginning phase two..." << endL;
      phaseTwo();
      args.logg << "Beginning phase three..." << endL;
      phaseThree();
   }

   void addS( set< fEdge, fEdgeLT >::iterator ee ) {
      ee->inS = true;
      fEdge eee( ee->x, ee->y );
      args.S.push_back( eee );
      args.g.remove_edge( ee->x, ee->y );
   }
  
   /*
    * Process a path means:
    * 1. Check if it is valid
    * 2. Determine if x*(e) + x*(f) < 1 / T
    * 3. If so, add edge g \in p into S, where g is argmax x*(g)
    * 
    */
   void process( NodePath& p ) {
      double min1 = 1.0; //smallest
      double min2 = 1.0; //second smallest
      double max = 0.0;
      set< fEdge, fEdgeLT >::iterator max_id;

      if (p.edges.size() <= 2) {
	 for (size_t i = 0; i < p.edges.size(); ++i) {
	    if (p.edges[i]->inS) {
	       //This path has already been broken
	       return;
	    }
	 }

	 //Not broken yet, break it
	 addS( p.edges[0] );
	 return;
      }
    
      for (size_t i = 0; i < p.edges.size(); ++i) {
	 if (p.edges[i]->inS) {
	    //This path has already been broken
	    return;
	 }

	 if (p.edges[i]->w <= min1) {
	    min2 = min1;
	    min1 = p.edges[i]->w;
	 } else {
	    if (p.edges[i]->w < min2)
	       min2 = p.edges[i]->w;
	 }

	 if (max < p.edges[i]->w) {
	    max = p.edges[i]->w;
	    max_id = p.edges[i];
	 }
      }

      //Having reached this point, know
      //path is unbroken by S, and
      //min1, min2 contain two smallest
      //x* values
      double thresh = 1.0 / (args.T - 1);
      if (min1 + min2 < thresh) {
	 //Break this path by including edge max_id
	 addS( max_id );
	 return;
      } else {
	 //This path remains unbroken
	 //Add it into Ph3Paths
	 Ph3Paths.push_back( p );
      }
   }

   void phaseThree() {
      set< fEdge, fEdgeLT >::iterator e;
      //Break all remaining paths
      for (size_t i = 0; i < Ph3Paths.size(); ++i) {
	 NodePath& p = Ph3Paths[i];
	 e = p.edges.back(); //last edge in path
	 if (!(e->inS)) {
	    addS( e );
	 }
      }
   }
  
  
   void phaseTwo() {
      //Go through each path, and process it
      for (size_t i = 0; i < allPaths.size(); ++i) {
	 process( allPaths[i] );
      }
   }
  
   void phaseOne() {
      allPaths.clear();
      vector< vector< Pair > > Pdiv (args.nThreads);
      vector< vector< NodePath > > PathsPart( args.nThreads ); 
      vector< Pair >::iterator itBegin, itEnd;
      itBegin = args.P.begin();
      thread* enum_threads = new thread[ args.nThreads ];
      for (size_t i = 0; i < args.nThreads; ++i) {
	 if (i == args.nThreads - 1) {
	    //Give rest of P to thread i
	    itEnd = args.P.end();
	 } else {
	    itEnd = itBegin + ((args.P.size())/args.nThreads);
	 }

	 Pdiv[i].assign( itBegin, itEnd );

	 enum_threads[i] = thread( enumBoundedMulti, ref( Pdiv[i] ), ref( PathsPart[i] ), ref( args.g ), args.T );
	 itBegin = itEnd;
      }

      for (size_t i = 0; i < args.nThreads; ++i) {
	 enum_threads[i].join();
	 allPaths.insert( allPaths.end(), PathsPart[i].begin(), PathsPart[i].end() );      
      }

      delete [] enum_threads;
    
      for (size_t i = 0; i < allPaths.size(); ++i) {
	 //Add edges of all valid paths to 'edges'
	 for (size_t j = 0; j < allPaths[i].nodes.size() - 1; ++j) {
	    unsigned char w = args.g.getEdgeWeight( allPaths[i].nodes[j], allPaths[i].nodes[j + 1] );
	    fEdge ee( allPaths[i].nodes[j], allPaths[i].nodes[j + 1], w );
	    pair< set< fEdge, fEdgeLT>::iterator, bool> ret = edges.insert( ee );
	    allPaths[i].edges.push_back( ret.first );
	 }
      }

      vEdges.assign( edges.begin(), edges.end() );
      solve_lp();
   }

   size_t getIndex( const node_id& x, const node_id& y ) {
      fEdge e(x,y);
      for (size_t i = 0; i < vEdges.size(); ++i) {
	 if ( e == vEdges[i] )
	    return i;
      }

      return 0;
   }
  
   void solve_lp() {
      tinyGraph& g = args.g;

      IloEnv env;
      IloModel model(env);
      IloNumVarArray var(env);
      IloRangeArray con(env);

      for (size_t i = 0; i < vEdges.size(); ++i) {
	 var.add( IloNumVar( env ) );
      }

      // IloExpr edge_obj( env, 0 );
      // for (size_t i = 0; i < vEdges.size(); ++i) {
      //   edge_obj += var[i];
      // }

      //    model.add( IloMinimize( env, edge_obj ) );

      //Add constraints corresponding to each path
      IloRangeArray c(env);
      IloObjective obj = IloMinimize( env );
    
      for (size_t i = 0; i < allPaths.size(); ++i) {
	 c.add( IloRange( env, 1.0, IloInfinity ) );
      }

      for (size_t i = 0; i < vEdges.size(); ++i) {
	 obj.setLinearCoef( var[ i ], 1.0 );
      }

      for (size_t i = 0; i < allPaths.size(); ++i) {
	 NodePath& apath = allPaths[i];
	 for (size_t j = 0; j < apath.nodes.size() - 1; ++j) {
	    size_t k = getIndex( apath.nodes[j], apath.nodes[j + 1] );
	    c[ i ].setLinearCoef( var[ k ], 1.0 );
	 }
      }

      model.add(obj);
      model.add(c);

      IloCplex cplex( model );
      cplex.setParam(IloCplex::IntParam::Threads, args.nThreads);
      cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
      // //Tells cplex to use wall-clock time
      // cplex.setParam(IloCplex::ClockType, 2); 
      // double hour = 60.0 * 60;
      // cplex.setParam(IloCplex::TiLim, max_hours * hour );
      if (!args.bVerbose)
	 cplex.setOut(env.getNullStream());
    
      cplex.solve();
      set< fEdge, fEdgeLT >::iterator it = edges.begin();
      for (size_t i = 0; i < vEdges.size(); ++i ) {
	 it->w = cplex.getValue(var[i]);
	 if (it->x != vEdges[i].x || it->y != vEdges[i].y) {
	    args.logg << ERROR << "Edges inconsistent in lp_solve()" << endL;
	    args.logg << INFO;
	 }
	 ++it;
      }

   }
};

//////////Sampling alg.
typedef map< fEdge, double, fEdgeLT > Est;

/*
 * Samples a path, biased by a Dijkstra computation
 * Returns probability of path if valid, and 0 otherwise
 */
double samplePath( NodePath& p, node_id& s, node_id& t, Dijkstra& dij, uint32_t& T, tinyGraph& g, double& gamma ) {
   double prob = 1.0;
   p.length = 0;
   p.nodes.clear();
   p.nodes.push_back( s );
   p.fedges.clear();
   node_id curr = s; //Current node of path
   vector< node_id > available_neis;
   vector< uint32_t > nei_weights;
  
   //double gamma = 0.75;
   uniform_real_distribution< double > realDist(0.0,1.0);
   while (curr != t) {
      //take a step
      //with prob. gamma, make that step to the node indicated in dij
      if (realDist(gen) < gamma) {
	 //step to node indicated in Dijkstra, if that step is valid
	 node_id& dnext = dij.forw[ curr ];
	 vector< tinyEdge >::iterator adjIt = g.adjList[ curr ].incident( dnext );
	 if (adjIt != g.adjList[curr].neis.end()) {
	    if (!p.ctns(dnext)) {
	       prob = prob * gamma;
	       p.addNode( dnext, adjIt->weight );
	       p.fedges.push_back( fEdge( curr, dnext ) );
	       curr = dnext;
	       if (p.length > T)
		  return 0.0;

	       continue;
	    } else {
	  
	    }
	 }
      } else {
	 prob = prob * (1.0 - gamma);
      }

      //Need to take a random step now
      //Find available vertices
      available_neis.clear();
      nei_weights.clear();
    
      for (size_t j = 0; j < g.adjList[ curr ].neis.size(); ++j) {
	 node_id anei = g.adjList[ curr ].neis[ j ].getId();
	 tinyEdge& enei = g.adjList[ curr ].neis[ j ];
	 if (p.length + enei.weight <= T) {
	    if (!p.ctns( anei )) {
	       //This neighbor is available
	       available_neis.push_back( anei );
	       nei_weights.push_back( enei.weight );
	    }
	 }
      }

      if (available_neis.size() == 0) {
	 //Have reached a dead end
	 return 0.0;
      }

      uniform_int_distribution< size_t > neiDist(0, available_neis.size() - 1);
      size_t neiIndex = neiDist( gen );
      p.addNode( available_neis[ neiIndex ], nei_weights[ neiIndex ] );
      p.fedges.push_back( fEdge( curr, available_neis[ neiIndex ] ) );
      curr = available_neis[ neiIndex ];
      prob = prob * (1.0 / available_neis.size() );
   }

   //At this point, we have sampled a valid path
   return prob;
}

/* 
 * Samples L T-bounded paths in g from s to t (i.e. makes L attempts)
 * Returns estimate of #paths each edge lies upon
 *
 * Helper function
 * for Gsamp class
 */
void sampleWorker( tinyGraph& g,
		   Est& est,
		   size_t L,
		   node_id s,
		   node_id t,
		   uint32_t T,
		   Dijkstra& dij,
		   double& gamma ) {
   //First, need to know how far to t from each node u
   //= dij.h[u]

   //  Dijkstra dij( s, t, g );
   //  dij.compute_dij();
   NodePath p;
   double prob;

   for (size_t i = 0; i < L; ++i) {
      //Sample a path
      prob = samplePath( p, s, t, dij, T, g, gamma );
      if (prob > 0.0) {
	 //We have sampled a valid path!
	 //update the estimator
	 for (size_t j = 0; j < p.fedges.size(); ++j) {
	    Est::iterator itm = est.find( p.fedges[ j ] );
	    if (itm != est.end()) {
	       //Edge already has nonzero value
	       itm->second += 1.0 / prob;
	    } else {
	       //Edge had zero value
	       est[ p.fedges[ j ] ] = 1.0 / prob;
	    }
	 }
      }
   }

   // cerr << "contents of est: " << endl;
   // Est::iterator it = est.begin();
   // while (it != est.end()){
   //   cerr << (it->first).x << ' ' << (it->first).y << ' ' << it->second << endl;
   //   ++it;
   // }
    
}

void startWorkers( map< fEdge, Dijkstra, fEdgeLT >::iterator itstart,
		   map< fEdge, Dijkstra, fEdgeLT >::iterator itend,
		   Est& myEst,
		   tinyGraph& g, size_t L, uint32_t T, double& gamma ) {
   while (itstart != itend) {
      sampleWorker( g, myEst, L, (itstart->first).x, (itstart->first).y, T, (itstart->second), gamma );
      ++itstart;
   }
}

class Gsamp {
public:
   Args& args;
   map< fEdge, Dijkstra, fEdgeLT > mdist;
   double initialMaxWeight = 0.0;
   size_t optUpperBound = 0;
   size_t L; //number of samples per pair
   double gamma = 0.75;
   
   void getOptUpperBound() {
      Args PDargs( args ); //copy args for PD
      {
	 PDargs.alg = PRIMALDUAL;
	 Darp pd( PDargs );
	 pd.run();
	 optUpperBound = PDargs.S.size();
      }
      args.logg << "Upper bound on opt is: " << optUpperBound << endL;
   }
   
   Gsamp( Args& arg ) : args( arg ) {
      args.logg << "Getting upper bound on OPT from PRIMAL-DUAL..." << endL;
      getOptUpperBound();
      
      args.logg << "Performing initial Dijkstra computations..." << endL;

      vector< Dijkstra > dij;
    
      for (size_t i = 0; i < args.nPairs; ++i) {
	 Dijkstra d1( args.P[i].x, args.P[i].y, args.g );
	 dij.push_back( d1 );
      }

      cdArgs cdargs;
      cdargs.T = args.T;
      cdWorker cdworker;

      parallelWork( dij,
		    cdargs,
		    cdworker,
		    args.nThreads );
      for (size_t i = 0; i < args.nPairs; ++i) {
	 if (dij[i].dst <= args.T) {
	    //Need to consider this pair
	    mdist.insert( pair< fEdge, Dijkstra >(args.P[i], dij[i]));
	 }
      }
   }
  
   bool addEdge() {
      //size_t Delta = 0;
      /*
       * Get an upper bound on OPT
       * upper bound is now computed with PD
       */ 
      // for ( auto it = mdist.begin(); it != mdist.end(); ++it ) {
      // 	 node_id& s = (it->second).s;
      // 	 node_id& t = (it->second).t;
      // 	 if ( args.g.adjList[ s ].neis.size() > Delta ) {
      // 	    Delta = args.g.adjList[ s ].neis.size();
      // 	 }
      // 	 if ( args.g.adjList[ t ].neis.size() > Delta ) {
      // 	    Delta = args.g.adjList[ t ].neis.size();
      // 	 }
      // }


      map< fEdge, Dijkstra, fEdgeLT >::iterator itBegin = mdist.begin();
      map< fEdge, Dijkstra, fEdgeLT >::iterator itEnd;

      if (mdist.size() < args.nThreads) {
	 args.nThreads = mdist.size();
      }

      vector< Est > vEst( args.nThreads );
      
      thread* worker_threads = new thread[ args.nThreads ];
      for (size_t j = 0; j < args.nThreads; ++j) {
	 if (j == args.nThreads - 1) {
	    itEnd = mdist.end();
	 } else {
	    itEnd = itBegin;
	    size_t k = 0;
	    while (k < mdist.size() / args.nThreads) {
	       ++k;
	       ++itEnd;
	    }
	 }
      
	 worker_threads[ j ] = thread(
				      startWorkers, itBegin, itEnd,
				      ref( vEst[ j ] ),
				      ref( args.g ),
				      L,
				      args.T,
				      ref( gamma ));

	 itBegin = itEnd;
      }

      for (size_t j = 0; j < args.nThreads; ++j) {
	 worker_threads[j].join();
      }
      delete [] worker_threads;

      //Have done all sampling. Combine results and choose edge
      map < fEdge, double, fEdgeLT > totalWeight;
      map < fEdge, double, fEdgeLT >::iterator itTW;
      Est::iterator ite;
      for (size_t j = 0; j < vEst.size(); ++j) {
	 ite = vEst[j].begin();
	 while (ite != vEst[j].end()) {
	    itTW = totalWeight.find( ite->first );
	    if (itTW == totalWeight.end()) {
	       //Edge currently has a weight of 0
	       totalWeight[ ite->first ] = ite->second;
	    } else {
	       //Edge has a weight, so add the new weight to it
	       itTW->second += ite->second;
	    }
	    ++ite;
	 }
      }

      //Now just want maximum edge in totalWeight
      double max = 0.0;
      fEdge emax;
      itTW = totalWeight.begin();
      while (itTW != totalWeight.end()) {
	 if (itTW->second > max) {
	    max = itTW->second;
	    emax = itTW->first;
	 }
      
	 ++itTW;
      }

      if (initialMaxWeight == 0.0) {
	 if (max != 0.0) {
	    initialMaxWeight = max;
	    //Add this edge to S!
	    args.S.push_back( emax );
	    args.g.remove_edge( emax.x, emax.y );
	    return true;
	 } else {
	    //Not sampling any valid paths...
	    //Double the number of samples...
	    //	    L = L * 2;
	    //Force a shortest path sample
	    gamma = 0.99;
	    return false;
	 }
      } else {
	 if (max < 0.1 * initialMaxWeight ) {
	    args.logg << WARN << "Max edge weight is small...looks like few valid paths were sampled" << endL;
	    args.logg << INFO;
	    initialMaxWeight = 0.0;
	    return false;
	 } else {
	    //Add this edge to S!
	    args.S.push_back( emax );
	    args.g.remove_edge( emax.x, emax.y );
	    // don't reset. gamma = 0.75; // reset gamma in case it was boosted
	    return true;
	 }
      }

      return false;
   }

   void recomputeDists( bool update ) {
      struct wkArgs {
	 bool bUpdate;
	 uint32_t T;
      } wkargs;

      wkargs.bUpdate = update;
      wkargs.T = args.T;
    
      class wkWork {
      public:
	 static void run(
			 map< fEdge, Dijkstra, fEdgeLT >::iterator itBegin,
			 map< fEdge, Dijkstra, fEdgeLT >::iterator itEnd,
			 wkArgs& wkargs,
			 mutex& mtx ) {
	    while (itBegin != itEnd) {
	       if (wkargs.bUpdate)
		  (itBegin->second).update_Astar();
	       else
		  (itBegin->second).compute_h( wkargs.T + 1 );
	       ++itBegin;
	    }
	 }
      
      } wkwork;

      parallelWork( mdist, wkargs, wkwork, args.nThreads );

      map< fEdge, Dijkstra, fEdgeLT >::iterator it = mdist.begin();
    
      uint32_t dst;
      while (it != mdist.end()) {
	 dst = (it->second).dst;
      
	 if (dst > args.T) {
	    //May remove this element
	    it = mdist.erase( it );
	 } else {
	    ++it;
	 }
      }
   }
  
   void run() {
      L = optUpperBound * log( args.g.m ) / log ( 10 );//optUpperBound * optUpperBound * log( args.g.m ) / log ( 10 );
      if (L < 100)
	 L = 100;
      args.logg << "L = " << L << endL;
      while (mdist.size() > 0) {
	 args.logg << "Pairs left: " << mdist.size() << endL;
	 if (addEdge()) {
	    //args.logg << "recomputing dists..." << endL;
	    //Allow updating the distance quickly
	    recomputeDists( true );
	 } else {
	    args.logg << "Having trouble sampling valid paths, redoing Dijkstra hints..." << endL;
	    //Having trouble sampling valid paths, redo Dijkstra hints
	    recomputeDists( false );
	 }
      }
   }
};

struct mapCompare {
   bool operator()( const set< fEdge, fEdgeLT>::iterator& e1,
		    const set< fEdge, fEdgeLT>::iterator& e2 ) {
      fEdgeLT f;
      return f( *e1, *e2 );
   }
};

class Gen {
public:
   Args& args;
   vector < NodePath > allPaths;
   vector < fEdge > vEdges;
   set< fEdge, fEdgeLT> edges;
   map< set< fEdge, fEdgeLT>::iterator, size_t, mapCompare > eCount;

   Gen( Args& arg ) : args( arg ) { }

   void run() {
      allPaths.clear();
      vector< vector< Pair > > Pdiv (args.nThreads);
      vector< vector< NodePath > > PathsPart( args.nThreads ); 
      vector< Pair >::iterator itBegin, itEnd;
      itBegin = args.P.begin();
      thread* enum_threads = new thread[ args.nThreads ];
      for (size_t i = 0; i < args.nThreads; ++i) {
	 if (i == args.nThreads - 1) {
	    //Give rest of P to thread i
	    itEnd = args.P.end();
	 } else {
	    itEnd = itBegin + ((args.P.size())/args.nThreads);
	 }

	 Pdiv[i].assign( itBegin, itEnd );

	 enum_threads[i] = thread( enumBoundedMulti, ref( Pdiv[i] ), ref( PathsPart[i] ), ref( args.g ), args.T );
	 itBegin = itEnd;
      }

      for (size_t i = 0; i < args.nThreads; ++i) {
	 enum_threads[i].join();
	 allPaths.insert( allPaths.end(), PathsPart[i].begin(), PathsPart[i].end() );      
      }

      delete [] enum_threads;
    
      set< fEdge, fEdgeLT > edges;
    
      for (size_t i = 0; i < allPaths.size(); ++i) {
	 //      allPaths[i].print( cout );
	 //Add edges of all valid paths to 'edges'
	 for (size_t j = 0; j < allPaths[i].nodes.size() - 1; ++j) {
	    fEdge ee( allPaths[i].nodes[j], allPaths[i].nodes[j + 1] );
	    pair< set< fEdge, fEdgeLT>::iterator, bool> ret = edges.insert( ee );
	    allPaths[i].edges.push_back( ret.first );
	 }
      }

      while( allPaths.size() > 0 ) {
	 set< fEdge, fEdgeLT>::iterator e = getBestEdge();
	 args.g.remove_edge( e->x, e->y );
	 args.S.push_back( *e );
	 removePaths( e );
      }
   }

   void removePaths( set< fEdge, fEdgeLT>::iterator e ) {
      vector< NodePath > newPaths;
      bool eInPath;
      for (size_t i = 0; i < allPaths.size(); ++i) {
	 eInPath = false;
	 for (size_t j = 0; j < allPaths[i].edges.size(); ++j) {
	    if ( allPaths[i].edges[j] == e ) {
	       eInPath = true;
	       break;
	    }
	 }
	 if (!eInPath)
	    newPaths.push_back( allPaths[i] );
      }
      allPaths.swap( newPaths );
   }
  
   set< fEdge, fEdgeLT>::iterator getBestEdge() {
      set< fEdge, fEdgeLT>::iterator itb = edges.begin();
      eCount.clear();
      while (itb != edges.end()) {
	 eCount[ itb ] = 0;
	 ++itb;
      }
    
      for (size_t i = 0; i < allPaths.size(); ++i) {
	 for (size_t j = 0; j < allPaths[i].edges.size(); ++j) {
	    eCount[ allPaths[i].edges[j] ] = eCount[ allPaths[i].edges[j] ] + 1;
	 }
      }

      //Get Max
      size_t cmax = 0;
      set< fEdge, fEdgeLT>::iterator emax;
      map< set< fEdge, fEdgeLT>::iterator, size_t, mapCompare >::iterator it2 = eCount.begin();
      while (it2 != eCount.end()) {
	 if (it2->second > cmax) {
	    cmax = it2->second;
	    emax = it2->first;
	 }
	 ++it2;
      }

      //    cerr << cmax << endl;
    
      return emax;
    
   }

};

/////BnB
class solNode {
public:
   vector< bool > partialSol;
   size_t level;
   bool left_child;
   size_t size;

   
   
   solNode() {
      level = 0;
      left_child = false;
      size = 0;
   }
  
   solNode( vector< bool >& parentSol, size_t parentLevel, bool inLeftChild, size_t pSize )
      : partialSol( parentSol.begin(), parentSol.end() ) {
      level = parentLevel + 1;
      left_child = inLeftChild;
      if (left_child) {
	 size = pSize;
	 partialSol.push_back( false );
      } else {
	 size = pSize + 1;
	 partialSol.push_back( true );
      }
   }

   solNode (const solNode& rhs ) : partialSol( rhs.partialSol.begin(), rhs.partialSol.end() ),
				   level( rhs.level ), left_child( rhs.left_child), size( rhs.size ) {
   }

  
};

struct bWorkArgs {
   vector< fEdge > edges;
   vector< tinyGraph > vG;
   deque< solNode > Qprime;
   size_t minSize;
};

class bWorker {
public:
   tinyGraph myG;
   vector< Dijkstra > dij;
   Args& args;
   vector< fEdge >& edges;
  
   bWorker( const bWorker& rhs ) : myG( rhs.myG ),
				   args( rhs.args ),
				   edges( rhs.edges )
   {
      size_t initBound = 0;
      for (size_t i = 0; i < args.nPairs; ++i) {
	 Dijkstra d1( args.P[i].x, args.P[i].y, myG );
	 dij.push_back( d1 );
	 initBound = initBound + args.g.getDegree( args.P[i].x ) + args.g.getDegree( args.P[i].y );
      }

      args.logg( INFO, "Initializing Dijkstra objects..." );
      cdArgs cdargs;
      cdargs.T = args.T;
      cdWorker cdworker;
      parallelWork( dij,
		    cdargs,
		    cdworker,
		    1 );
   }
  
   bWorker( Args& aargs, vector< fEdge >& eedges, tinyGraph& g ) : args( aargs ), edges( eedges ) {
      myG.assign( g );
      args.logg( INFO, "Creating Dijkstra objects..." );
      size_t initBound = 0;
      for (size_t i = 0; i < args.nPairs; ++i) {
	 Dijkstra d1( args.P[i].x, args.P[i].y, myG );
	 dij.push_back( d1 );
	 initBound = initBound + args.g.getDegree( args.P[i].x ) + args.g.getDegree( args.P[i].y );
      }

      args.logg( INFO, "Initializing Dijkstra objects..." );
      cdArgs cdargs;
      cdargs.T = args.T;
      cdWorker cdworker;
      parallelWork( dij,
		    cdargs,
		    cdworker,
		    1 );
   }
  
   /*
    * Returns iterator to Dijkstra object
    * of first pair violating d(x,y) > T,
    * Returns dij.end() if no violations
    */
   vector<Dijkstra>::iterator detectViol( bool update_h = true, bool useDijk = false ) {
      bool bFound = false;
      dvArgs dvargs(bFound, update_h, useDijk );
      dvargs.T = args.T;
      dvWorker dvworker;
      parallelWork( dij,
		    dvargs,
		    dvworker,
		    1 );

      if (dvargs.bFound)
	 return dvargs.itViol;

      return dij.end();
   }
  
   void removeS( vector<bool>& partialSol ) {
      for (size_t i = 0; i < partialSol.size(); ++i) {
	 if (partialSol[i] == true) {
	    myG.remove_edge( edges[i] );
	 }
      }
   }
  
   void addS( vector<bool>& partialSol ) {
      for (size_t i = 0; i < partialSol.size(); ++i) {
	 if (partialSol[i] == true)
	    myG.add_edge( edges[i] );
      }
   }

   bool feasibleBelow( vector<bool>& partialSol ) {
      bool isFeasible;

      //Remove all additio%nal edges
      for (size_t i = partialSol.size(); i < edges.size(); ++i) {
	 myG.remove_edge( edges[i] );
      }
      isFeasible = (detectViol(false) == dij.end());
      //Add back additional edges
      for (size_t i = partialSol.size(); i < edges.size(); ++i) {
	 myG.add_edge( edges[i] );
      }

      return isFeasible;
   }

};

void bNbWork(
	     deque< solNode >::iterator itBegin,
	     deque< solNode >::iterator itEnd,
	     bWorker& bwrk,
	     size_t& minSize,
	     vector< bool >& minSol,
	     deque< solNode >& Qprime,
	     mutex& mtx
	     ) {

   deque< solNode > myQ;
   while (itBegin != itEnd) {
      solNode& curr = *itBegin;
      if (curr.size >= minSize) {
	 ++itBegin;
	 continue;
      }
      //update graph to remove S
      bwrk.removeS( curr.partialSol );

      //If we are a left child, we need to see if we have
      //a feasible branch.
      if (curr.left_child) {
	 if (!(bwrk.feasibleBelow( curr.partialSol ) ) ) {
	    //Prune this branch, no feasible solutions below
	    bwrk.addS( curr.partialSol );
	    ++itBegin;
	    continue;
	 }
      } else { //An edge was added to S here
	 //See if the partial solution is feasible.
	 //If it is, no need to continue down this branch
	 if (bwrk.detectViol( false ) == bwrk.dij.end()) {
	    //Solution is feasible
	    mtx.lock();
	    if (curr.size < minSize) {
	       minSize = curr.size;
	       minSol = curr.partialSol;
	    }
	    mtx.unlock();
	  
	    bwrk.addS( curr.partialSol );
	    ++itBegin;
	    continue;
	 }
      }

      //Further work is needed on this branch
      //Split
      //Left child
      solNode lc( curr.partialSol, curr.level, true, curr.size );
      myQ.push_back( lc );
      //Right child
      solNode rc( curr.partialSol, curr.level, false, curr.size );
      myQ.push_back( rc );
      
      bwrk.addS( curr.partialSol );
      ++itBegin;
   }

   //This thread is finished, add results to Qprime
   mtx.lock();
   Qprime.insert( Qprime.end(), myQ.begin(), myQ.end() );
   mtx.unlock();
}

class BnB {
public:
   vector< fEdge > edges;
   vector < NodePath > allPaths;
   vector< bWorker > bWork;

   tinyGraph gRedux;
   
   Args& args;
   vector< Dijkstra > dij;
   size_t minSize;
   vector< bool > minSol;
   vector< fEdge > Spreproc;

   BnB( Args& arrgs ) : args( arrgs ) {
      preprocess();
      
      //Creates reduced graph
      //from relevant edges only
      //      populateEdgesByEnum();

      populateEdges();
      
      args.logg( INFO, "Creating master Dijkstra objects..." );
      size_t initBound = 0;
      for (size_t i = 0; i < args.nPairs; ++i) {
      
	 Dijkstra d1( args.P[i].x, args.P[i].y, gRedux );
	 dij.push_back( d1 );
	 initBound = initBound + args.g.getDegree( args.P[i].x ) + args.g.getDegree( args.P[i].y );
      }

      args.logg( INFO, "Initializing master Dijkstra objects..." );
      cdArgs cdargs;
      cdargs.T = args.T;
      cdWorker cdworker;
      parallelWork( dij,
		    cdargs,
		    cdworker,
		    args.nThreads );
      
      minSize = initBound + 1;
      
      getDarpBound();

      vector< fEdge > newEdges = args.S;
      for (size_t i = 0; i < edges.size(); ++i) {
	 if (!ctnsEdge( newEdges, edges[i] ) ) {
	    newEdges.push_back( edges[i] );
	 }
      }

      edges.swap( newEdges );
      for (size_t i = 0; i < args.S.size(); ++i) {
	 minSol.push_back( true );
      }

      args.S.clear(); args.W.clear();

      args.logg << "Initializing workers..." << endL;
      for (size_t i = 0; i < args.nThreads; ++i) {
	 bWorker bb( args, edges, gRedux );
	 bWork.push_back( bb );
      }
    
      args.logg << "Initial bound: " << minSize << endL;
   }

   void preprocess() {
      // args.logg << "Preprocessing..." << endL;
      // //The greedy solution is exact if paths have at most 2 hops
      // Args argsGen;
      // argsGen.g.assign( args.g );
      // argsGen.T = 2;
      // argsGen.nThreads = args.nThreads;
      // Gen gen( argsGen );
      // gen.run();
      // Spreproc = argsGen.S;
      // for (size_t i = 0; i < argsGen.S.size(); ++i) {
      // 	 args.g.remove_edge( argsGen.S[i] );
      // }

      // args.logg << "Preprocessing removed " << argsGen.S.size() << " edges." << endL;

      //      for (size_t i = 0; i < allPaths.size(); ++i) {
      //	 for (size_t j = 0; j < allPaths.size(); ++j ) {
	    
      //	 }
      //      }
   }

   void getDarpBound() {
      //Get better initial bound. Use DARP
      bool bVerboseSaved = args.bVerbose;
      args.bVerbose = false;
    
      Darp darp( args );
      darp.run();

      removePruned( args );
    
      if (minSize > args.S.size())
	 minSize = args.S.size();
    
      for (size_t i = 0; i < args.S.size(); ++i) {
	 args.g.add_edge( args.S[i] );
      }

      args.bVerbose = bVerboseSaved;
   }
  
   void populateEdgesByEnum() {
      args.logg << "Finding candidate edges by path enumeration..." << endL;
    
      allPaths.clear();
      vector< vector< Pair > > Pdiv (args.nThreads);
      vector< vector< NodePath > > PathsPart( args.nThreads ); 
      vector< Pair >::iterator itBegin, itEnd;
      itBegin = args.P.begin();
      thread* enum_threads = new thread[ args.nThreads ];
      for (size_t i = 0; i < args.nThreads; ++i) {
	 if (i == args.nThreads - 1) {
	    //Give rest of P to thread i
	    itEnd = args.P.end();
	 } else {
	    itEnd = itBegin + ((args.P.size())/args.nThreads);
	 }

	 Pdiv[i].assign( itBegin, itEnd );

	 enum_threads[i] = thread( enumBoundedMulti, ref( Pdiv[i] ), ref( PathsPart[i] ), ref( args.g ), args.T );
	 itBegin = itEnd;
      }

      for (size_t i = 0; i < args.nThreads; ++i) {
	 enum_threads[i].join();
	 allPaths.insert( allPaths.end(), PathsPart[i].begin(), PathsPart[i].end() );      
      }

      delete [] enum_threads;
    
      set< fEdge, fEdgeLT > edgesSet;
    
      for (size_t i = 0; i < allPaths.size(); ++i) {
	 //      allPaths[i].print( cout );
	 //Add edges of all valid paths to 'edges'
	 for (size_t j = 0; j < allPaths[i].nodes.size() - 1; ++j) {
	    unsigned char w = args.g.getEdgeWeight( allPaths[i].nodes[j], allPaths[i].nodes[j + 1] );
	    fEdge ee( allPaths[i].nodes[j], allPaths[i].nodes[j + 1], w );
	    edgesSet.insert( ee );
	 }
      }

      set< fEdge, fEdgeLT >::iterator itS = edgesSet.begin();
      while (itS != edgesSet.end()) {
	 fEdge eTmp( *itS );
	 if (!ctnsEdge( edges, eTmp))
	    edges.push_back( fEdge(*itS) );
	 ++itS;
      }
      //    edges.insert( edges.end(), edgesSet.begin(), edgesSet.end() );
      args.logg << "Number of edges, paths: " << edges.size() << " " << allPaths.size() << endL;
      args.logg << "Creating reduced graph..." << endL;
      gRedux.n = args.g.n;
      gRedux.init_empty_graph();

      for (size_t i = 0; i < edges.size(); ++i) {
	 gRedux.add_edge( edges[i] );
      }
   }

   void populateEdges() {
      tinyGraph& g = args.g;
      edges = args.S;
      for (node_id i = 0; i < g.n; ++i) {
	 tinyNode& I = g.adjList[i];
	 for (size_t j = 0; j < I.neis.size(); ++j) {
	    node_id nei = I.neis[j].getId();
	    if (i < nei) {
	       fEdge e( i, nei, I.neis[j].weight );
	       edges.push_back( e );
	    }
	 }
      }

      gRedux.assign( g );
   }

   /*
    * Returns iterator to Dijkstra object
    * of first pair violating d(x,y) > T,
    * Returns dij.end() if no violations
    */
   vector<Dijkstra>::iterator detectViol( bool update_h = true, bool useDijk = false ) {
      bool bFound = false;
      dvArgs dvargs(bFound, update_h, useDijk );
      dvargs.T = args.T;
      dvWorker dvworker;
      parallelWork( dij,
		    dvargs,
		    dvworker,
		    args.nThreads );

      if (dvargs.bFound)
	 return dvargs.itViol;

      return dij.end();
   }

   void removeS( vector<bool>& partialSol ) {
      for (size_t i = 0; i < partialSol.size(); ++i) {
	 if (partialSol[i] == true)
	    args.g.remove_edge( edges[i] );
      }
   }

   void addS( vector<bool>& partialSol ) {
      for (size_t i = 0; i < partialSol.size(); ++i) {
	 if (partialSol[i] == true)
	    args.g.add_edge( edges[i] );
      }
   }

   bool feasibleBelow( vector<bool>& partialSol ) {
      bool isFeasible;

      //Remove all additional edges
      for (size_t i = partialSol.size(); i < edges.size(); ++i) {
	 args.g.remove_edge( edges[i] );
      }
      isFeasible = (detectViol(false) == dij.end());
      //Add back additional edges
      for (size_t i = partialSol.size(); i < edges.size(); ++i) {
	 args.g.add_edge( edges[i] );
      }

      return isFeasible;
   }

   void run() {
      args.logg << "Beginning branch and bound procedure..." << endL;
      size_t level = 0;
      deque< solNode > Q;
      deque< solNode > Qprime;
    
      solNode start;
      Q.push_back( start );
      size_t nThreads;

      while (!Q.empty()) {
	 args.logg << "Level: " << level << ", active solnodes: " << Q.size()
		   << ", minSize: " << minSize << endL;
	 Qprime.clear();
      
	 nThreads = args.nThreads;
	 if (Q.size() < nThreads) {
	    nThreads = Q.size();
	 }

	 //Split the work on the level
	 auto itBegin = Q.begin();
	 auto itEnd = Q.end();

	 thread* wThreads = new thread[ nThreads ];
	 mutex mtx;

	 for (size_t i = 0; i < nThreads; ++i) {
	    if (i == nThreads - 1) {
	       itEnd = Q.end();
	    } else {
	       itEnd = itBegin;
	       for (size_t j = 0; j < (Q.size())/nThreads; ++j)
		  ++itEnd;
	  
	    }

	    wThreads[i] = thread( bNbWork,
				  itBegin,
				  itEnd,
				  ref( bWork[i] ),
				  ref( minSize ),
				  ref( minSol ),
				  ref( Qprime ),
				  ref( mtx ) );
	    itBegin = itEnd;
	 }

	 for (size_t i = 0; i < nThreads; ++i) {
	    wThreads[i].join();
	 }

	 delete [] wThreads;

	 //Qprime has the next level of Q
	 Q.swap( Qprime );
	 ++level;
      }

      for (size_t i = 0; i < minSol.size(); ++i) {
	 if (minSol[i]) {
	    args.S.push_back( edges[i] );
	 }
      }

      for (size_t i = 0; i < Spreproc.size(); ++i) {
	 args.S.push_back( Spreproc[i] );
      }
   }
   void single_run() {
      args.logg << "Beginning branch and bound procedure..." << endL;
      size_t level = 0;
      queue< solNode > Q;
      tinyGraph& g = args.g;
      solNode start;
      Q.push( start );
      while (!Q.empty()) {
	 solNode curr = Q.front();
	 if (curr.size >= minSize) {
	    //No need to consider this branch
	    Q.pop();
	    continue;
	 }

	 if (curr.level > level) {
	    level = curr.level;
	    args.logg << "Level: " << level << ", active solnodes: " << Q.size() << endL;
	 }
      
	 //update graph to remove S
	 removeS( curr.partialSol );

	 //If we are a left child, we need to see if we have
	 //a feasible branch.
	 if (curr.left_child) {

	    if (!(feasibleBelow( curr.partialSol ) ) ) {
	       //Prune this branch, no feasible solutions below
	       addS( curr.partialSol );
	       Q.pop();
	       continue;
	    }
	 }
     
	 //See if the partial solution is feasible.
	 //If it is, no need to continue down this branch
	 if (detectViol( false ) == dij.end()) {
	    //Solution is feasible
	    minSize = curr.size;
	    minSol = curr.partialSol;
	
	    addS( curr.partialSol );
	    Q.pop();
	    continue;
	 }

	 //Further work is needed on this branch
	 //Split
	 //Left child
	 solNode lc( curr.partialSol, curr.level, true, curr.size );
	 Q.push( lc );
	 //Right child
	 solNode rc( curr.partialSol, curr.level, false, curr.size );
	 Q.push( rc );
      
	 addS( curr.partialSol );
	 Q.pop();
      }

      for (size_t i = 0; i < minSol.size(); ++i) {
	 if (minSol[i]) {
	    args.S.push_back( edges[i] );
	 }
      }
   }
};

struct miaDijArgs {
   uint32_t T;
};

class miaDijWorker {
public:
   static void run(
		   vector< Dijkstra >::iterator itBegin,
		   vector< Dijkstra >::iterator itEnd,
		   miaDijArgs& args,
		   mutex& mtx ) {
      while (itBegin != itEnd) {
	 itBegin->compute_dij_all_paths( args.T + 1 );
	 ++itBegin;
      }
   }
};

typedef vector< node_id > MiaPath;

class Mia {
public:
   Args& args;
   map< fEdge, Dijkstra, fEdgeLT > mdist;
   vector< vector< node_id > > compatSet;
   vector< node_id > emptyVector;
   
   Mia( Args& arg ) : args( arg ) {
      args.logg << "Starting MIA..." << endL;
      // args.logg << "Performing shortest-path computations..." << endL;

      vector< Dijkstra > dij;
    
      for (size_t i = 0; i < args.nPairs; ++i) {
       	 Dijkstra d1( args.P[i].x, args.P[i].y, args.g );
	 dij.push_back( d1 );
      }

      // miaDijArgs cdargs;
      // cdargs.T = args.T;
      // miaDijWorker cdworker;

      // parallelWork( dij,
      // 		    cdargs,
      // 		    cdworker,
      // 		    args.nThreads );
      for (size_t i = 0; i < args.nPairs; ++i) {
	 //if (dij[i].dst <= args.T) {
	    //Need to consider this pair
	    mdist.insert( pair< fEdge, Dijkstra >(args.P[i], dij[i]));
	    //}
      }

      recomputeDists();
      
      args.logg << "done." << endL;
   }

   bool compatible ( MiaPath& p ) {
      for (size_t i = 0; i < p.size(); ++i) {
	 node_id& curr = p[i];
	 if (vector_ctns( compatSet[ i ], curr ))
	    continue;
	 
	 for (size_t j = 0; j < i; ++j) {
	    if ( vector_ctns( compatSet[ j ], curr ) ) {
	       return false;
	    }
	 }

	 for (size_t j = i + 1; j < args.T + 1; ++j) {
	    if ( vector_ctns( compatSet[ j ], curr ) ) {
	       return false;
	    }
	 }

      }

      //Having reached this point,
      //The path is compatible
      //Add it
      for (size_t i = 0; i < p.size(); ++i) {
	 if (!vector_ctns( compatSet[ i ], p[i] )) {
	    compatSet[i].push_back( p[i] );
	 }
      }
      return true;
   }

   void recomputeDists() {
      args.logg << "Performing shortest-path computations..." << endL;
      struct wkArgs {
	 uint32_t T;
      } wkargs;

      wkargs.T = args.T;
    
      class wkWork {
      public:
	 static void run(
			 map< fEdge, Dijkstra, fEdgeLT >::iterator itBegin,
			 map< fEdge, Dijkstra, fEdgeLT >::iterator itEnd,
			 wkArgs& wkargs,
			 mutex& mtx ) {
	    while (itBegin != itEnd) {
	       (itBegin->second).compute_dij_all_paths( wkargs.T + 1 );
	       ++itBegin;
	    }
	 }
      
      } wkwork;

      parallelWork( mdist, wkargs, wkwork, args.nThreads );

      map< fEdge, Dijkstra, fEdgeLT >::iterator it = mdist.begin();
    
      uint32_t dst;
      while (it != mdist.end()) {
	 dst = (it->second).dst;
      
	 if (dst > args.T) {
	    //May remove this element
	    it = mdist.erase( it );
	 } else {
	    ++it;
	 }
      }
   }

   void getAllShortest( list< MiaPath >& Pshort ) {
      Pshort.clear();
      for (auto it = mdist.begin(); it != mdist.end(); ++it) {
	 Dijkstra& D = it->second;
	 queue < MiaPath > Q; //queue of partial paths
	 MiaPath T( 1, D.t );
	 Q.push( T );
	 while (!Q.empty()) {
	    //cerr << Q.size() << endl;
	    MiaPath& part = Q.front();
	    //newPath.assign ( part.begin(), part.end() );
	    node_id curr = *(part.end() - 1);

	    if (curr == D.s) {
	       //This path is done
	       reverse( part.begin(), part.end() );
	       Pshort.push_back( part );
	    } else {
	       if (D.all_prev[ curr ].size() == 1) {
		  //No need to pop part off and push it to the back...
		  part.push_back( D.all_prev[ curr ][0] );
		  continue;
	       }
	       
	       for (size_t i = 0; i < D.all_prev[ curr ].size(); ++i) {
		  node_id next = D.all_prev[curr][i];
		  MiaPath newPath( part.begin(), part.end() );
		  newPath.push_back( next );
		  Q.push( newPath );
	       }
	    }

	    Q.pop();
	 }
      }

      
   }

   void minDirMCut( vector< MiaPath >& Q ) {
      mdmc::Args argcut;
      tinyGraph& H = argcut.g;
      argcut.P = args.P;
      H.n = args.g.n;
      H.m = 0;
      H.init_empty_graph();
      for (size_t i = 0; i < Q.size(); ++i) {
	 MiaPath& p = Q[i];
	 for (size_t j = 0; j < p.size() - 1; ++j) {
	    vector< tinyEdge >::iterator tmp;
	    if ( H.add_edge_half( p[j], p[j + 1], tmp ) ) {
	       ++H.m;
	    }
	 }
      }

      argcut.nThreads = args.nThreads;
      argcut.bVerbose = args.bVerbose;
      mdmc::mdmcType MDMC( argcut );

      if (args.bMiaOpt) {
	 MDMC.opt();
      } else {
	 MDMC.gupta();
      }
      
      //remove multi-cut edges from our graph
      for (size_t i = 0; i < argcut.S.size(); ++i) {
	 if ( args.g.remove_edge_half( argcut.S[i].x, argcut.S[i].y ) ) {
	    args.S.push_back( argcut.S[i] );
	 }
      }
   }


   
   void run() {
      size_t M = 0;
      
      do {
	 list < MiaPath > Pshort;
	 args.logg << "Getting all shortest paths..." << endL;
	 getAllShortest( Pshort );
	 vector< MiaPath > Q;

	 args.logg << "Starting multi-cut phase..." << endL;
	 while (Pshort.size() > 0) {
	    compatSet.assign( args.T + 1, emptyVector );
	    Q.clear();
	    auto it = Pshort.begin();
	    while ( it != Pshort.end() ) {
	       if ( compatible ( *it ) ) {
		  Q.push_back( *it );
		  it = Pshort.erase( it );
	       } else {
		  ++it;
	       }
	    }
	    ++M;

	    args.logg << "Starting multi-cut " << M << "..." << endL;
	    minDirMCut( Q );;
	    
	 }
	 
	 recomputeDists();
      } while ( mdist.size() > 0 );

      args.logg << "MIA finished, number of multi-cuts: " << M << endL;
      args.miaCuts = M;
      args.logg << "Size of solution: " << args.S.size() << endL;
   }
   
};

#endif
