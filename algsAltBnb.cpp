#ifndef ALGS_CPP
#define ALGS_CPP

#include "mygraph.cpp"
#include <ilcplex/ilocplex.h>
#include <set>
#include <map>
#include <string>
#include <vector>
#include <fstream>

using namespace std;
using namespace mygraph;

enum Algs {DARP, OPT, PARL, GSAMP, GEN, BNB};

typedef fEdge Pair;

struct Args {
  Algs alg;
  string graphFileName;
  string pairFileName;
  size_t nPairs;
  vector < Pair > P;
  tinyGraph g;
  uint32_t T;
  vector< fEdge > S;
  vector< fEdge > W;
  Logger logg;
  double tElapsed;
  bool bVerbose = false;
  bool bFeasible;
  unsigned nThreads = 1;
};

bool ctnsEdge( vector< fEdge >& v, fEdge& e ) {
  for (size_t i = 0; i < v.size(); ++i) {
    if ( e == v[i] )
      return true;
  }

  return false;
}

void removePruned( Args& args ) {
  vector< fEdge > S;
  for (size_t i = 0; i < args.S.size(); ++i) {
    if ( !ctnsEdge( args.W, args.S[i] ) ) {
      S.push_back( args.S[i] );
    }
  }
  args.S.swap( S );
}

void enumBoundedMulti( vector< Pair >& Pi, vector< NodePath >& iPaths, tinyGraph& g, uint32_t T ) {
  iPaths.clear();
  for (size_t i = 0; i < Pi.size(); ++i) {
    vector< NodePath > stPaths;
    enumBoundedPaths( stPaths, Pi[i].x, Pi[i].y, g, T );
    iPaths.insert( iPaths.end(), stPaths.begin(), stPaths.end() );
  }
}

/*
 * Reads target pairs from file
 * args.pairFileName
 * Format is number of pairs, followed
 * by all of the pairs, plain text
 *
 */
void readPairs( Args& args ) {
  args.P.clear();
  if (args.pairFileName == "") {
    cerr << "No filename given for target pairs...\n";
    exit( 2 );
  }
   
  ifstream ifile( args.pairFileName.c_str() );
  size_t nPairs;
  ifile >> nPairs;
  Pair tmp;
  for (size_t i = 0; i < nPairs; ++i) {
    ifile >> tmp.x;
    ifile >> tmp.y;
    args.P.push_back( tmp );
  }

  args.nPairs = nPairs;
  ifile.close();
}

struct dvArgs {
  bool& bFound;
  vector< Dijkstra >::iterator itViol; //Iterator to a dijkstra object violating feasibility
  uint32_t T;
  bool update_h;
  bool useDijk;
  
  dvArgs( bool& bf, bool update_h_in = true, bool useDijk_in = false ):
    bFound( bf ), update_h( update_h_in ), useDijk( useDijk_in )  {
  }
  
};

struct dvWorker{
  static void run(
		  vector< Dijkstra >::iterator itBegin,
		  vector< Dijkstra >::iterator itEnd,
		  dvArgs& args,
		  mutex& mtx
		  ) {
    uint32_t tmpDst;
    while (itBegin != itEnd) {
      if (args.bFound) //Check to see if we are already done
	return;
      if (!args.useDijk) {
	if ( (itBegin->t_elapsed > itBegin->t_dij) && args.update_h) {
	  tmpDst = itBegin->compute_h( args.T + 1 );
	  tmpDst = itBegin->update_Astar();
	} else {
	  tmpDst = itBegin->update_Astar();
	}
      } else {
	tmpDst = itBegin->compute_dij( args.T + 1 );
      }
      
      if (tmpDst <= args.T) {
	mtx.lock();
	args.bFound = true;
	args.itViol = itBegin;
	mtx.unlock();
	return;
      }
      ++itBegin;
    }

    return;
  }
};

struct cdArgs {
  uint32_t T;
};

class cdWorker {
public:
  static void run(
		  vector< Dijkstra >::iterator itBegin,
		  vector< Dijkstra >::iterator itEnd,
		  cdArgs& args,
		  mutex& mtx ) {
    while (itBegin != itEnd) {
      itBegin->compute_h( args.T + 1 );
      itBegin->save_h();
      ++itBegin;
    }
  }
};


class Darp {
public:
  const uint32_t undefined = ~( static_cast< uint32_t >(0) );
  Args& args;
  vector< Dijkstra > dij;

  Darp( Args& aargs ) : args( aargs ) 
  {
    args.logg( INFO, "Creating Dijkstra objects..." );
    for (size_t i = 0; i < args.nPairs; ++i) {
      Dijkstra d1( args.P[i].x, args.P[i].y, args.g );
      dij.push_back( d1 );
    }

    args.logg( INFO, "Initializing Dijkstra objects..." );
    cdArgs cdargs;
    cdargs.T = args.T;
    cdWorker cdworker;
    parallelWork( dij,
		  cdargs,
		  cdworker,
		  args.nThreads );
  }

  /*
   * Returns iterator to Dijkstra object
   * of first pair violating d(x,y) > T,
   * Returns dij.end() if no violations
   */
  vector<Dijkstra>::iterator detectViol( bool update_h = true, bool useDijk = false ) {
    bool bFound = false;
    dvArgs dvargs(bFound, update_h, useDijk );
    dvargs.T = args.T;
    dvWorker dvworker;
    parallelWork( dij,
		  dvargs,
		  dvworker,
		  args.nThreads );

    if (dvargs.bFound)
      return dvargs.itViol;

    return dij.end();
  }

  void run() {
    args.logg(INFO, "Beginning removal phase..." );
    vector<Dijkstra>::iterator itViol = dij.begin();//detectViol( true );

    while (itViol != dij.end()) {
      itViol->update_Astar();
      while ( itViol -> dst <= args.T ) {
	if (args.bVerbose) {
	  args.logg << "Current pair: ("
		    << itViol->s << "," << itViol->t << "): "
		    << itViol->dst << endL;
	  args.logg << "Removing shortest path..." << endL;
	}

	rmPath( itViol );

	if (args.bVerbose) {
	  args.logg << "Updating distance..." << endL;
	}
	
	if ( (itViol->t_elapsed > itViol->t_dij) ) {
	  itViol->compute_h( args.T + 1 );
	  itViol->update_Astar();
	} else {
	  itViol->update_Astar();
	}
      }

      ++itViol;
    }

    prune();
  }

  /* Removes shortest path between violating pair
   * as computed by update_Astar
   * from graph and adds it
   * to the solution args.S
   */
  void rmPath( vector<Dijkstra>::iterator itViol ) {
    fEdge e;
    e.x = itViol->t;
     
     
    do {
      e.y = e.x;
      e.x = itViol->prev[ e.y ];
      e.w = static_cast< double >( args.g.remove_edge( e.x, e.y ) );

      args.S.push_back( e );
    } while ( e.x != itViol->s );
  }

  void prune() {
    args.logg( INFO, "Pruning solution..." );

    //Restore the original h values
    if (args.bVerbose)
      args.logg << "Restoring h values..." << endL;
    
    for (size_t i = 0; i < dij.size(); ++i) {
      dij[i].restore_h();
    }
    
    vector<Dijkstra>::iterator itViol;
    for (size_t i = 0; i < args.S.size(); ++i) {
      args.g.add_edge( args.S[i].x, args.S[i].y, static_cast<unsigned char>(args.S[i].w) );
      if (args.bVerbose) {
	args.logg << args.S.size() - i << " elements left. ";
	args.logg << "Attempting to reinsert edge (" << args.S[i].x << "," << args.S[i].y << ")..." << endL;
      }
      itViol = detectViol( false, false );
      if ( itViol != dij.end() ) {
	//Edge required for feasibility, remove it again
	args.g.remove_edge( args.S[i].x, args.S[i].y );
	if (args.bVerbose)
	  args.logg << "Feasibility violated, could not reinsert." << endL;
      } else {
	//Edge can be pruned
	args.W.push_back( args.S[i] );
      }      
    }
  }
  
};

class Opt {
public:
  Args& args;
  vector < NodePath > allPaths;
  vector < fEdge > vEdges;
  Opt( Args& arg ) : args( arg ) { }
  
  void run() {
    allPaths.clear();
    vector< vector< Pair > > Pdiv (args.nThreads);
    vector< vector< NodePath > > PathsPart( args.nThreads ); 
    vector< Pair >::iterator itBegin, itEnd;
    itBegin = args.P.begin();
    thread* enum_threads = new thread[ args.nThreads ];
    for (size_t i = 0; i < args.nThreads; ++i) {
      if (i == args.nThreads - 1) {
	//Give rest of P to thread i
	itEnd = args.P.end();
      } else {
	itEnd = itBegin + ((args.P.size())/args.nThreads);
      }

      Pdiv[i].assign( itBegin, itEnd );

      enum_threads[i] = thread( enumBoundedMulti, ref( Pdiv[i] ), ref( PathsPart[i] ), ref( args.g ), args.T );
      itBegin = itEnd;
    }

    for (size_t i = 0; i < args.nThreads; ++i) {
      enum_threads[i].join();
      allPaths.insert( allPaths.end(), PathsPart[i].begin(), PathsPart[i].end() );      
    }

    delete [] enum_threads;
    
    set< fEdge, fEdgeLT > edges;
    
    for (size_t i = 0; i < allPaths.size(); ++i) {
      //      allPaths[i].print( cout );
      //Add edges of all valid paths to 'edges'
      for (size_t j = 0; j < allPaths[i].nodes.size() - 1; ++j) {
	fEdge ee( allPaths[i].nodes[j], allPaths[i].nodes[j + 1] );
	edges.insert( ee );
      }
    }

    //    cout << allPaths.size() << endl;
    //    cout << edges.size() << endl;

    vEdges.assign( edges.begin(), edges.end() );
    //    cout << vEdges.size() << endl;

    solve_ip();
  }

  size_t getIndex( const node_id& x, const node_id& y ) {
    fEdge e(x,y);
    for (size_t i = 0; i < vEdges.size(); ++i) {
      if ( e == vEdges[i] )
	return i;
    }

    return 0;
  }
  
  void solve_ip() {
    tinyGraph& g = args.g;

    IloEnv env;
    IloModel model(env);
    IloNumVarArray var(env);
    IloRangeArray con(env);

    for (size_t i = 0; i < vEdges.size(); ++i) {
      var.add( IloNumVar( env, 0.0, 1.0, ILOINT ) );
    }

    // IloExpr edge_obj( env, 0 );
    // for (size_t i = 0; i < vEdges.size(); ++i) {
    //   edge_obj += var[i];
    // }

    //    model.add( IloMinimize( env, edge_obj ) );

    //Add constraints corresponding to each path
    IloRangeArray c(env);
    IloObjective obj = IloMinimize( env );
    
    for (size_t i = 0; i < allPaths.size(); ++i) {
      c.add( IloRange( env, 1.0, IloInfinity ) );
    }

    for (size_t i = 0; i < vEdges.size(); ++i) {
      obj.setLinearCoef( var[ i ], 1.0 );
    }

    for (size_t i = 0; i < allPaths.size(); ++i) {
      NodePath& apath = allPaths[i];
      for (size_t j = 0; j < apath.nodes.size() - 1; ++j) {
	size_t k = getIndex( apath.nodes[j], apath.nodes[j + 1] );
	c[ i ].setLinearCoef( var[ k ], 1.0 );
      }
    }

    model.add(obj);
    model.add(c);

    IloCplex cplex( model );
    cplex.setParam(IloCplex::IntParam::Threads, args.nThreads);
    cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
    // //Tells cplex to use wall-clock time
    // cplex.setParam(IloCplex::ClockType, 2); 
    // double hour = 60.0 * 60;
    // cplex.setParam(IloCplex::TiLim, max_hours * hour );
    if (!args.bVerbose)
      cplex.setOut(env.getNullStream());
    
    cplex.solve();

    for (size_t i = 0; i < vEdges.size(); ++i ) {
      if (cplex.getValue(var[i] == 1)) {
	g.remove_edge( vEdges[i].x, vEdges[i].y );
	args.S.push_back( vEdges[ i ] );
      }
    }
  }
};

class Parl {
public:
  Args& args;
  vector< NodePath > allPaths;
  set< fEdge, fEdgeLT > edges;
  vector< fEdge > vEdges;
  vector< NodePath > Ph3Paths;
  
  Parl( Args& arg ) : args( arg ) { }
  
  void run() {
    args.logg << "Beginning phase one..." << endL;
    phaseOne();
    args.logg << "Beginning phase two..." << endL;
    phaseTwo();
    args.logg << "Beginning phase three..." << endL;
    phaseThree();
  }

  void addS( set< fEdge, fEdgeLT >::iterator ee ) {
    ee->inS = true;
    fEdge eee( ee->x, ee->y );
    args.S.push_back( eee );
    args.g.remove_edge( ee->x, ee->y );
  }
  
  /*
   * Process a path means:
   * 1. Check if it is valid
   * 2. Determine if x*(e) + x*(f) < 1 / T
   * 3. If so, add edge g \in p into S, where g is argmax x*(g)
   * 
   */
  void process( NodePath& p ) {
    double min1 = 1.0; //smallest
    double min2 = 1.0; //second smallest
    double max = 0.0;
    set< fEdge, fEdgeLT >::iterator max_id;

    if (p.edges.size() <= 2) {
      addS( p.edges[0] );
      return;
    }
    for (size_t i = 0; i < p.edges.size(); ++i) {
      if (p.edges[i]->inS) {
	//This path has already been broken
	return;
      }

      if (p.edges[i]->w <= min1) {
	min2 = min1;
	min1 = p.edges[i]->w;
      } else {
	if (p.edges[i]->w < min2)
	  min2 = p.edges[i]->w;
      }

      if (max < p.edges[i]->w) {
	max = p.edges[i]->w;
	max_id = p.edges[i];
      }
    }

    //Having reached this point, know
    //path is unbroken by S, and
    //min1, min2 contain two smallest
    //x* values
    double thresh = 1.0 / (args.T - 1);
    if (min1 + min2 < thresh) {
      //Break this path by including edge max_id
      addS( max_id );
      return;
    } else {
      //This path remains unbroken
      //Add it into Ph3Paths
      Ph3Paths.push_back( p );
    }
  }

  void phaseThree() {
    set< fEdge, fEdgeLT >::iterator e;
    //Break all remaining paths
    for (size_t i = 0; i < Ph3Paths.size(); ++i) {
      NodePath& p = Ph3Paths[i];
      e = p.edges.back(); //last edge in path
      if (!(e->inS)) {
	addS( e );
      }
    }
  }
  
  
  void phaseTwo() {
    //Go through each path, and process it
    for (size_t i = 0; i < allPaths.size(); ++i) {
      process( allPaths[i] );
    }
  }
  
  void phaseOne() {
    allPaths.clear();
    vector< vector< Pair > > Pdiv (args.nThreads);
    vector< vector< NodePath > > PathsPart( args.nThreads ); 
    vector< Pair >::iterator itBegin, itEnd;
    itBegin = args.P.begin();
    thread* enum_threads = new thread[ args.nThreads ];
    for (size_t i = 0; i < args.nThreads; ++i) {
      if (i == args.nThreads - 1) {
	//Give rest of P to thread i
	itEnd = args.P.end();
      } else {
	itEnd = itBegin + ((args.P.size())/args.nThreads);
      }

      Pdiv[i].assign( itBegin, itEnd );

      enum_threads[i] = thread( enumBoundedMulti, ref( Pdiv[i] ), ref( PathsPart[i] ), ref( args.g ), args.T );
      itBegin = itEnd;
    }

    for (size_t i = 0; i < args.nThreads; ++i) {
      enum_threads[i].join();
      allPaths.insert( allPaths.end(), PathsPart[i].begin(), PathsPart[i].end() );      
    }

    delete [] enum_threads;
    
    for (size_t i = 0; i < allPaths.size(); ++i) {
      //Add edges of all valid paths to 'edges'
      for (size_t j = 0; j < allPaths[i].nodes.size() - 1; ++j) {
	fEdge ee( allPaths[i].nodes[j], allPaths[i].nodes[j + 1] );
	pair< set< fEdge, fEdgeLT>::iterator, bool> ret = edges.insert( ee );
	allPaths[i].edges.push_back( ret.first );
      }
    }

    vEdges.assign( edges.begin(), edges.end() );
    solve_lp();
  }

  size_t getIndex( const node_id& x, const node_id& y ) {
    fEdge e(x,y);
    for (size_t i = 0; i < vEdges.size(); ++i) {
      if ( e == vEdges[i] )
	return i;
    }

    return 0;
  }
  
  void solve_lp() {
    tinyGraph& g = args.g;

    IloEnv env;
    IloModel model(env);
    IloNumVarArray var(env);
    IloRangeArray con(env);

    for (size_t i = 0; i < vEdges.size(); ++i) {
      var.add( IloNumVar( env ) );
    }

    // IloExpr edge_obj( env, 0 );
    // for (size_t i = 0; i < vEdges.size(); ++i) {
    //   edge_obj += var[i];
    // }

    //    model.add( IloMinimize( env, edge_obj ) );

    //Add constraints corresponding to each path
    IloRangeArray c(env);
    IloObjective obj = IloMinimize( env );
    
    for (size_t i = 0; i < allPaths.size(); ++i) {
      c.add( IloRange( env, 1.0, IloInfinity ) );
    }

    for (size_t i = 0; i < vEdges.size(); ++i) {
      obj.setLinearCoef( var[ i ], 1.0 );
    }

    for (size_t i = 0; i < allPaths.size(); ++i) {
      NodePath& apath = allPaths[i];
      for (size_t j = 0; j < apath.nodes.size() - 1; ++j) {
	size_t k = getIndex( apath.nodes[j], apath.nodes[j + 1] );
	c[ i ].setLinearCoef( var[ k ], 1.0 );
      }
    }

    model.add(obj);
    model.add(c);

    IloCplex cplex( model );
    cplex.setParam(IloCplex::IntParam::Threads, args.nThreads);
    cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
    // //Tells cplex to use wall-clock time
    // cplex.setParam(IloCplex::ClockType, 2); 
    // double hour = 60.0 * 60;
    // cplex.setParam(IloCplex::TiLim, max_hours * hour );
    if (!args.bVerbose)
      cplex.setOut(env.getNullStream());
    
    cplex.solve();
    set< fEdge, fEdgeLT >::iterator it = edges.begin();
    for (size_t i = 0; i < vEdges.size(); ++i ) {
      it->w = cplex.getValue(var[i]);
      if (it->x != vEdges[i].x || it->y != vEdges[i].y) {
	args.logg << ERROR << "Edges inconsistent in lp_solve()" << endL;
	args.logg << INFO;
      }
      ++it;
    }

  }
};

//////////Sampling alg.
typedef map< fEdge, double, fEdgeLT > Est;

/*
 * Samples a path, biased by a Dijkstra computation
 * Returns probability of path if valid, and 0 otherwise
 */
double samplePath( NodePath& p, node_id& s, node_id& t, Dijkstra& dij, uint32_t& T, tinyGraph& g ) {
  double prob = 1.0;
  p.length = 0;
  p.nodes.clear();
  p.nodes.push_back( s );
  p.fedges.clear();
  node_id curr = s; //Current node of path
  vector< node_id > available_neis;
  vector< uint32_t > nei_weights;
  
  double gamma = 0.75;
  uniform_real_distribution< double > realDist(0.0,1.0);
  while (curr != t) {
    //take a step
    //with prob. gamma, make that step to the node indicated in dij
    if (realDist(gen) < gamma) {
      //step to node indicated in Dijkstra, if that step is valid
      node_id& dnext = dij.forw[ curr ];
      vector< tinyEdge >::iterator adjIt = g.adjList[ curr ].incident( dnext );
      if (adjIt != g.adjList[curr].neis.end()) {
	if (!p.ctns(dnext)) {
	  prob = prob * gamma;
	  p.addNode( dnext, adjIt->weight );
	  p.fedges.push_back( fEdge( curr, dnext ) );
	  curr = dnext;
	  if (p.length > T)
	    return 0.0;

	  continue;
	} else {
	  
	}
      }
    } else {
      prob = prob * (1.0 - gamma);
    }

    //Need to take a random step now
    //Find available vertices
    available_neis.clear();
    nei_weights.clear();
    
    for (size_t j = 0; j < g.adjList[ curr ].neis.size(); ++j) {
      node_id anei = g.adjList[ curr ].neis[ j ].getId();
      tinyEdge& enei = g.adjList[ curr ].neis[ j ];
      if (p.length + enei.weight <= T) {
	if (!p.ctns( anei )) {
	  //This neighbor is available
	  available_neis.push_back( anei );
	  nei_weights.push_back( enei.weight );
	}
      }
    }

    if (available_neis.size() == 0) {
      //Have reached a dead end
      return 0.0;
    }

    uniform_int_distribution< size_t > neiDist(0, available_neis.size() - 1);
    size_t neiIndex = neiDist( gen );
    p.addNode( available_neis[ neiIndex ], nei_weights[ neiIndex ] );
    p.fedges.push_back( fEdge( curr, available_neis[ neiIndex ] ) );
    curr = available_neis[ neiIndex ];
    prob = prob * (1.0 / available_neis.size() );
  }

  //At this point, we have sampled a valid path
  return prob;
}

/* 
 * Samples L T-bounded paths in g from s to t (i.e. makes L attempts)
 * Returns estimate of #paths each edge lies upon
 *
 * Helper function
 * for Gsamp class
 */
void sampleWorker( tinyGraph& g,
		   Est& est,
		   size_t L,
		   node_id s,
		   node_id t,
		   uint32_t T,
		   Dijkstra& dij ) {
  //First, need to know how far to t from each node u
  //= dij.h[u]

  //  Dijkstra dij( s, t, g );
  //  dij.compute_dij();
  NodePath p;
  double prob;

  for (size_t i = 0; i < L; ++i) {
    //Sample a path
    prob = samplePath( p, s, t, dij, T, g );
    if (prob > 0.0) {
      //We have sampled a valid path!
      //update the estimator
      for (size_t j = 0; j < p.fedges.size(); ++j) {
	Est::iterator itm = est.find( p.fedges[ j ] );
	if (itm != est.end()) {
	  //Edge already has nonzero value
	  itm->second += 1.0 / prob;
	} else {
	  //Edge had zero value
	  est[ p.fedges[ j ] ] = 1.0 / prob;
	}
      }
    }
  }

  // cerr << "contents of est: " << endl;
  // Est::iterator it = est.begin();
  // while (it != est.end()){
  //   cerr << (it->first).x << ' ' << (it->first).y << ' ' << it->second << endl;
  //   ++it;
  // }
    
}

void startWorkers( map< fEdge, Dijkstra, fEdgeLT >::iterator itstart,
		   map< fEdge, Dijkstra, fEdgeLT >::iterator itend,
		   Est& myEst,
		   tinyGraph& g, size_t L, uint32_t T ) {
  while (itstart != itend) {
    sampleWorker( g, myEst, L, (itstart->first).x, (itstart->first).y, T, (itstart->second) );
    ++itstart;
  }
}

class Gsamp {
public:
  Args& args;
  map< fEdge, Dijkstra, fEdgeLT > mdist;
  double initialMaxWeight = 0.0;
  
  Gsamp( Args& arg ) : args( arg ) {
    args.logg << "Performing initial Dijkstra computations..." << endL;

    vector< Dijkstra > dij;
    
    for (size_t i = 0; i < args.nPairs; ++i) {
      Dijkstra d1( args.P[i].x, args.P[i].y, args.g );
      dij.push_back( d1 );
    }

    cdArgs cdargs;
    cdargs.T = args.T;
    cdWorker cdworker;

    parallelWork( dij,
		  cdargs,
		  cdworker,
		  args.nThreads );
    for (size_t i = 0; i < args.nPairs; ++i) {
      if (dij[i].dst <= args.T) {
	//Need to consider this pair
	mdist.insert( pair< fEdge, Dijkstra >(args.P[i], dij[i]));
      }
    }
  }
  
  bool addEdge() {
    size_t L = 10000 * log( args.g.n ) / log ( 10 );
    args.logg << "L = " << L << endL;
    vector< Est > vEst( args.nThreads );
    map< fEdge, Dijkstra, fEdgeLT >::iterator itBegin = mdist.begin();
    map< fEdge, Dijkstra, fEdgeLT >::iterator itEnd;

    if (mdist.size() < args.nThreads) {
      args.nThreads = mdist.size();
    }
    
    thread* worker_threads = new thread[ args.nThreads ];
    for (size_t j = 0; j < args.nThreads; ++j) {
      if (j == args.nThreads - 1) {
	itEnd = mdist.end();
      } else {
	itEnd = itBegin;
	size_t k = 0;
	while (k < mdist.size() / args.nThreads) {
	  ++k;
	  ++itEnd;
	}
      }
      
      worker_threads[ j ] = thread(
				   startWorkers, itBegin, itEnd,
				   ref( vEst[ j ] ),
				   ref( args.g ),
				   L,
				   args.T );

      itBegin = itEnd;
    }

    for (size_t j = 0; j < args.nThreads; ++j) {
      worker_threads[j].join();
    }
    delete [] worker_threads;

    //Have done all sampling. Combine results and choose edge
    map < fEdge, double, fEdgeLT > totalWeight;
    map < fEdge, double, fEdgeLT >::iterator itTW;
    Est::iterator ite;
    for (size_t j = 0; j < vEst.size(); ++j) {
      ite = vEst[j].begin();
      while (ite != vEst[j].end()) {
	itTW = totalWeight.find( ite->first );
	if (itTW == totalWeight.end()) {
	  //Edge currently has a weight of 0
	  totalWeight[ ite->first ] = ite->second;
	} else {
	  //Edge has a weight, so add the new weight to it
	  itTW->second += ite->second;
	}
	++ite;
      }
    }

    //Now just want maximum edge in totalWeight
    double max = 0.0;
    fEdge emax;
    itTW = totalWeight.begin();
    while (itTW != totalWeight.end()) {
      if (itTW->second > max) {
	max = itTW->second;
	emax = itTW->first;
      }
      
      ++itTW;
    }

    if (initialMaxWeight == 0.0) {
      initialMaxWeight = max;
      //Add this edge to S!
      args.S.push_back( emax );
      args.g.remove_edge( emax.x, emax.y );
      return true;
    } else {
      if (max < 0.1 * initialMaxWeight ) {
	args.logg << WARN << "Max edge weight is small...looks like few valid paths were sampled" << endL;
	args.logg << INFO;
	initialMaxWeight = 0.0;
	return false;
      } else {
	//Add this edge to S!
	args.S.push_back( emax );
	args.g.remove_edge( emax.x, emax.y );
	return true;
      }
    }

    return false;
  }

  void recomputeDists( bool update ) {
    struct wkArgs {
      bool bUpdate;
      uint32_t T;
    } wkargs;

    wkargs.bUpdate = update;
    wkargs.T = args.T;
    
    class wkWork {
    public:
      static void run(
		      map< fEdge, Dijkstra, fEdgeLT >::iterator itBegin,
   		      map< fEdge, Dijkstra, fEdgeLT >::iterator itEnd,
		      wkArgs& wkargs,
		      mutex& mtx ) {
	while (itBegin != itEnd) {
	  if (wkargs.bUpdate)
	    (itBegin->second).update_Astar();
	  else
	    (itBegin->second).compute_h( wkargs.T + 1 );
	  ++itBegin;
	}
      }
      
    } wkwork;

    parallelWork( mdist, wkargs, wkwork, args.nThreads );

    map< fEdge, Dijkstra, fEdgeLT >::iterator it = mdist.begin();
    
    uint32_t dst;
    while (it != mdist.end()) {
      dst = (it->second).dst;
      
      if (dst > args.T) {
	//May remove this element
	it = mdist.erase( it );
      } else {
	++it;
      }
    }
  }
  
  void run() {
    while (mdist.size() > 0) {
      args.logg << "Pairs left: " << mdist.size() << endL;
      if (addEdge()) {
	//args.logg << "recomputing dists..." << endL;
	//Allow updating the distance quickly
	recomputeDists( true );
      } else {
	//	args.logg << "recomputing dists (no update)..." << endL;
	//Having trouble sampling valid paths, redo Dijkstra hints
	recomputeDists( false );
      }
    }
  }
};

struct mapCompare {
  bool operator()( const set< fEdge, fEdgeLT>::iterator& e1,
		   const set< fEdge, fEdgeLT>::iterator& e2 ) {
    fEdgeLT f;
    return f( *e1, *e2 );
  }
};

class Gen {
public:
  Args& args;
  vector < NodePath > allPaths;
  vector < fEdge > vEdges;
  set< fEdge, fEdgeLT> edges;
  map< set< fEdge, fEdgeLT>::iterator, size_t, mapCompare > eCount;

  Gen( Args& arg ) : args( arg ) { }

  void run() {
    allPaths.clear();
    vector< vector< Pair > > Pdiv (args.nThreads);
    vector< vector< NodePath > > PathsPart( args.nThreads ); 
    vector< Pair >::iterator itBegin, itEnd;
    itBegin = args.P.begin();
    thread* enum_threads = new thread[ args.nThreads ];
    for (size_t i = 0; i < args.nThreads; ++i) {
      if (i == args.nThreads - 1) {
	//Give rest of P to thread i
	itEnd = args.P.end();
      } else {
	itEnd = itBegin + ((args.P.size())/args.nThreads);
      }

      Pdiv[i].assign( itBegin, itEnd );

      enum_threads[i] = thread( enumBoundedMulti, ref( Pdiv[i] ), ref( PathsPart[i] ), ref( args.g ), args.T );
      itBegin = itEnd;
    }

    for (size_t i = 0; i < args.nThreads; ++i) {
      enum_threads[i].join();
      allPaths.insert( allPaths.end(), PathsPart[i].begin(), PathsPart[i].end() );      
    }

    delete [] enum_threads;
    
    set< fEdge, fEdgeLT > edges;
    
    for (size_t i = 0; i < allPaths.size(); ++i) {
      //      allPaths[i].print( cout );
      //Add edges of all valid paths to 'edges'
      for (size_t j = 0; j < allPaths[i].nodes.size() - 1; ++j) {
	fEdge ee( allPaths[i].nodes[j], allPaths[i].nodes[j + 1] );
	pair< set< fEdge, fEdgeLT>::iterator, bool> ret = edges.insert( ee );
	allPaths[i].edges.push_back( ret.first );
      }
    }

    while( allPaths.size() > 0 ) {
      set< fEdge, fEdgeLT>::iterator e = getBestEdge();
      args.g.remove_edge( e->x, e->y );
      args.S.push_back( *e );
      removePaths( e );
    }
  }

  void removePaths( set< fEdge, fEdgeLT>::iterator e ) {
    vector< NodePath > newPaths;
    bool eInPath;
    for (size_t i = 0; i < allPaths.size(); ++i) {
      eInPath = false;
      for (size_t j = 0; j < allPaths[i].edges.size(); ++j) {
	if ( allPaths[i].edges[j] == e ) {
	  eInPath = true;
	  break;
	}
      }
      if (!eInPath)
	newPaths.push_back( allPaths[i] );
    }
    allPaths.swap( newPaths );
  }
  
  set< fEdge, fEdgeLT>::iterator getBestEdge() {
    set< fEdge, fEdgeLT>::iterator itb = edges.begin();
    eCount.clear();
    while (itb != edges.end()) {
      eCount[ itb ] = 0;
      ++itb;
    }
    
    for (size_t i = 0; i < allPaths.size(); ++i) {
      for (size_t j = 0; j < allPaths[i].edges.size(); ++j) {
	eCount[ allPaths[i].edges[j] ] = eCount[ allPaths[i].edges[j] ] + 1;
      }
    }

    //Get Max
    size_t cmax = 0;
    set< fEdge, fEdgeLT>::iterator emax;
    map< set< fEdge, fEdgeLT>::iterator, size_t, mapCompare >::iterator it2 = eCount.begin();
    while (it2 != eCount.end()) {
      if (it2->second > cmax) {
	cmax = it2->second;
	emax = it2->first;
      }
      ++it2;
    }

    //    cerr << cmax << endl;
    
    return emax;
    
  }

};

/////BnB
class dijkSet {
public:
   vector < vector < uint32_t > > dist; 

   void init( tinyGraph& g, Args& args ) {
      dist.assign( args.nPairs, vector< uint32_t >() );

      //Compute d(s_i, u) for all u, and store in dist[i]
      for (size_t i = 0; i < args.nPairs; ++i) {
	 Dijkstra dij( args.P[i].x, args.P[i].y, g );
	 dij.compute_dij();
	 dist[i].swap( dij.dist );
      }
   }
   
   void updateAdd( tinyGraph& g, Args& args, fEdge& e ) {
      for (size_t i = 0; i < args.nPairs; ++i) {
	 Dijkstra dij( args.P[i].x, args.P[i].y, g );
	 dij.dist.swap( dist[i] );
	 dij.update_dij_after_addition( e );
	 dist[i].swap( dij.dist );
      }
   }

   uint32_t computeMin( Args& args ) {
      uint32_t dmin = uint32Max;
      for (size_t i = 0; i < args.nPairs; ++i) {
	 if ( dist[i][ args.P[i].y ] < dmin )
	    dmin = dist[i][ args.P[i].y ];
      }

      return dmin;
   }

   dijkSet() { }
   
   dijkSet( const dijkSet& rhs ) {
      dist.assign( rhs.dist.size(),vector<uint32_t>() );
      for (size_t i = 0; i < rhs.dist.size(); ++i) {
	 dist[i].assign( rhs.dist[i].begin(), rhs.dist[i].end() );
      }
   }

   dijkSet& operator=( const dijkSet& rhs ) {
      dist.assign( rhs.dist.size(), vector<uint32_t>() );
      for (size_t i = 0; i < rhs.dist.size(); ++i) {
	 dist[i].assign( rhs.dist[i].begin(), rhs.dist[i].end() );
      }
      return *this;
   }
};

class solNode {
public:
   vector< bool > partialSol;
   size_t level;
   bool left_child;
   size_t size;
   
   //Orig. graph with S removed. If distance > T here, no need to add any more edges to S
   tinyGraph LowerBound;
   //Empty graph with barS added (if distance is <= T in this graph, no feasible
   //solution with this partialSol exists)
   tinyGraph UpperBound;
   dijkSet upperSet;
   
   solNode( tinyGraph& g, Args& args ) {
    level = 0;
    left_child = false;
    size = 0;
    LowerBound.assign( g );
    UpperBound.n = g.n;
    UpperBound.init_empty_graph();
    upperSet.init( g, args );
  }
  
   solNode( vector< bool >& parentSol, size_t parentLevel, bool inLeftChild, size_t pSize, fEdge& e,
	    Args& args, tinyGraph& parentUpper, tinyGraph& parentLower, dijkSet& uS )
    : partialSol( parentSol.begin(), parentSol.end() ) {
      UpperBound.assign( parentUpper );
      LowerBound.assign( parentLower );
      level = parentLevel + 1;
    left_child = inLeftChild;
    upperSet = uS;
    if (left_child) {
      size = pSize;
      partialSol.push_back( false );
      UpperBound.add_edge( e );
      //      upperSet.updateAdd( UpperBound, args, e );
    } else {
      size = pSize + 1;
      partialSol.push_back( true );
      LowerBound.remove_edge( e );
    }
  }

  solNode (const solNode& rhs ) : partialSol( rhs.partialSol.begin(), rhs.partialSol.end() ),
				  level( rhs.level ), left_child( rhs.left_child), size( rhs.size ),
				  LowerBound( rhs.LowerBound ), UpperBound( rhs.UpperBound ),
				  upperSet( rhs.upperSet )
				  
   {
      
   }

   solNode& operator= (const solNode& rhs ) {
      partialSol = rhs.partialSol;
      level = rhs.level;
      left_child = rhs.left_child;
      LowerBound.assign( rhs.LowerBound );
      UpperBound.assign( rhs.UpperBound );
      upperSet = rhs.upperSet;

      return *this;
   }
};

void bNbWork(
	     deque< solNode >::iterator itBegin,
	     deque< solNode >::iterator itEnd,
	     size_t& minSize,
	     vector< bool >& minSol,
	     deque< solNode >& Qprime,
	     mutex& mtx,
	     Args& args,
	     vector< fEdge >& edges,
	     size_t level
		  ) {

    deque< solNode > myQ;
    while (itBegin != itEnd) {
      solNode& curr = *itBegin;
      if (curr.size >= minSize) {
	++itBegin;
	continue;
      }

      //If we are a left child, we need to see if we have
      //a feasible branch.
      if (curr.left_child) {
	 dijkSet dS;
	 dS.init( curr.UpperBound, args );
	 //if ( curr.upperSet.computeMin( args ) <= args.T ) {
	 if ( dS.computeMin( args ) <= args.T ) {
	  //Prune this branch, no feasible solutions below
	  ++itBegin;
	  continue;
	}
      } else {
	 //See if the partial solution is feasible.
	 //If it is, no need to continue down this branch
	 //This won't have changed if we are a left child
	 dijkSet dS;
	 dS.init( curr.LowerBound, args );
	 if ( dS.computeMin( args ) > args.T) {
	    //Solution is feasible
	    mtx.lock();
	    if (curr.size < minSize) {
	       minSize = curr.size;
	       minSol = curr.partialSol;
	    }
	    mtx.unlock();
	
	    ++itBegin;
	    continue;
	 }
      }

      //Further work is needed on this branch
      //Split
      //Left child
      solNode lc( curr.partialSol, curr.level, true, curr.size,
		  edges[ level ], args, curr.UpperBound, curr.LowerBound, curr.upperSet );
      myQ.push_back( lc );
      //Right child
      solNode rc( curr.partialSol, curr.level, false, curr.size,
		  edges[ level ], args, curr.UpperBound, curr.LowerBound, curr.upperSet );
      myQ.push_back( rc );

      ++itBegin;
    }

    //This thread is finished, add results to Qprime
    mtx.lock();
    Qprime.insert( Qprime.end(), myQ.begin(), myQ.end() );
    mtx.unlock();
}

class BnB {
public:
   vector< fEdge > edges;
   vector < NodePath > allPaths;

   tinyGraph gRedux;
   
   Args& args;
   vector< Dijkstra > dij;
   size_t minSize;
   vector< bool > minSol;
  

   BnB( Args& arrgs ) : args( arrgs ) {
      //Creates reduced graph
      //from relevant edges only
      populateEdgesByEnum();
      
      args.logg( INFO, "Creating master Dijkstra objects..." );
      size_t initBound = 0;
      for (size_t i = 0; i < args.nPairs; ++i) {
      
	 Dijkstra d1( args.P[i].x, args.P[i].y, gRedux );
	 dij.push_back( d1 );
	 initBound = initBound + args.g.getDegree( args.P[i].x ) + args.g.getDegree( args.P[i].y );
      }

      args.logg( INFO, "Initializing master Dijkstra objects..." );
      cdArgs cdargs;
      cdargs.T = args.T;
      cdWorker cdworker;
      parallelWork( dij,
		    cdargs,
		    cdworker,
		    args.nThreads );
      
      minSize = initBound + 1;
      
           getDarpBound();

      vector< fEdge > newEdges = args.S;
      for (size_t i = 0; i < edges.size(); ++i) {
      	 if (!ctnsEdge( newEdges, edges[i] ) ) {
      	    newEdges.push_back( edges[i] );
      	 }
      }

      edges.swap( newEdges );
      
      args.S.clear(); args.W.clear();

      args.logg << "Initial bound: " << minSize << endL;
  }

  void getDarpBound() {
    //Get better initial bound. Use DARP
    bool bVerboseSaved = args.bVerbose;
    args.bVerbose = false;
    
    Darp darp( args );
    darp.run();

    removePruned( args );
    
    if (minSize > args.S.size())
      minSize = args.S.size() + 1;
    
    for (size_t i = 0; i < args.S.size(); ++i) {
      args.g.add_edge( args.S[i] );
    }

    args.bVerbose = bVerboseSaved;
  }
  
  void populateEdgesByEnum() {
    args.logg << "Finding candidate edges by path enumeration..." << endL;
    
    allPaths.clear();
    vector< vector< Pair > > Pdiv (args.nThreads);
    vector< vector< NodePath > > PathsPart( args.nThreads ); 
    vector< Pair >::iterator itBegin, itEnd;
    itBegin = args.P.begin();
    thread* enum_threads = new thread[ args.nThreads ];
    for (size_t i = 0; i < args.nThreads; ++i) {
      if (i == args.nThreads - 1) {
	//Give rest of P to thread i
	itEnd = args.P.end();
      } else {
	itEnd = itBegin + ((args.P.size())/args.nThreads);
      }

      Pdiv[i].assign( itBegin, itEnd );

      enum_threads[i] = thread( enumBoundedMulti, ref( Pdiv[i] ), ref( PathsPart[i] ), ref( args.g ), args.T );
      itBegin = itEnd;
    }

    for (size_t i = 0; i < args.nThreads; ++i) {
      enum_threads[i].join();
      allPaths.insert( allPaths.end(), PathsPart[i].begin(), PathsPart[i].end() );      
    }

    delete [] enum_threads;
    
    set< fEdge, fEdgeLT > edgesSet;
    
    for (size_t i = 0; i < allPaths.size(); ++i) {
      //      allPaths[i].print( cout );
      //Add edges of all valid paths to 'edges'
      for (size_t j = 0; j < allPaths[i].nodes.size() - 1; ++j) {
	unsigned char w = args.g.getEdgeWeight( allPaths[i].nodes[j], allPaths[i].nodes[j + 1] );
	fEdge ee( allPaths[i].nodes[j], allPaths[i].nodes[j + 1], w );
	edgesSet.insert( ee );
      }
    }

    set< fEdge, fEdgeLT >::iterator itS = edgesSet.begin();
    while (itS != edgesSet.end()) {
      fEdge eTmp( *itS );
      if (!ctnsEdge( edges, eTmp))
	edges.push_back( fEdge(*itS) );
      ++itS;
    }
    //    edges.insert( edges.end(), edgesSet.begin(), edgesSet.end() );
    args.logg << "Number of edges: " << edges.size() << endL;
    args.logg << "Creating reduced graph..." << endL;
    gRedux.n = args.g.n;
    gRedux.init_empty_graph();

    for (size_t i = 0; i < edges.size(); ++i) {
       gRedux.add_edge( edges[i] );
    }
  }

  void populateEdges() {
    tinyGraph& g = args.g;
    edges = args.S;
    for (node_id i = 0; i < g.n; ++i) {
      tinyNode& I = g.adjList[i];
      for (size_t j = 0; j < I.neis.size(); ++j) {
	node_id nei = I.neis[j].getId();
	if (i < nei) {
	  fEdge e( i, nei, I.neis[j].weight );
	  edges.push_back( e );
	}
      }
    }
  }

  /*
   * Returns iterator to Dijkstra object
   * of first pair violating d(x,y) > T,
   * Returns dij.end() if no violations
   */
  vector<Dijkstra>::iterator detectViol( bool update_h = true, bool useDijk = false ) {
    bool bFound = false;
    dvArgs dvargs(bFound, update_h, useDijk );
    dvargs.T = args.T;
    dvWorker dvworker;
    parallelWork( dij,
		  dvargs,
		  dvworker,
		  args.nThreads );

    if (dvargs.bFound)
      return dvargs.itViol;

    return dij.end();
  }

  void removeS( vector<bool>& partialSol ) {
    for (size_t i = 0; i < partialSol.size(); ++i) {
      if (partialSol[i] == true)
	args.g.remove_edge( edges[i] );
    }
  }

  void addS( vector<bool>& partialSol ) {
    for (size_t i = 0; i < partialSol.size(); ++i) {
      if (partialSol[i] == true)
	args.g.add_edge( edges[i] );
    }
  }

  bool feasibleBelow( vector<bool>& partialSol ) {
    bool isFeasible;

    //Remove all additional edges
    for (size_t i = partialSol.size(); i < edges.size(); ++i) {
      args.g.remove_edge( edges[i] );
    }
    isFeasible = (detectViol(false) == dij.end());
    //Add back additional edges
    for (size_t i = partialSol.size(); i < edges.size(); ++i) {
      args.g.add_edge( edges[i] );
    }

    return isFeasible;
  }

  void run() {
    args.logg << "Beginning branch and bound procedure..." << endL;
    size_t level = 0;
    deque< solNode > Q;
    deque< solNode > Qprime;
    
    solNode start( gRedux, args );
    Q.push_back( start );
    size_t nThreads;

    while (!Q.empty()) {
      args.logg << "Level: " << level << ", active solnodes: " << Q.size() << endL;
      Qprime.clear();
      
      nThreads = args.nThreads;
      if (Q.size() < nThreads) {
	nThreads = Q.size();
      }

      //Split the work on the level
      auto itBegin = Q.begin();
      auto itEnd = Q.end();

      thread* wThreads = new thread[ nThreads ];
      mutex mtx;

      for (size_t i = 0; i < nThreads; ++i) {
	if (i == nThreads - 1) {
	  itEnd = Q.end();
	} else {
	  itEnd = itBegin;
	  for (size_t j = 0; j < (Q.size())/nThreads; ++j)
	    ++itEnd;
	  
	}

	wThreads[i] = thread( bNbWork,
			      itBegin,
			      itEnd,
			      ref( minSize ),
			      ref( minSol ),
			      ref( Qprime ),
			      ref( mtx ),
			      ref( args ),
			      ref( edges ),
			      level );
	   
	itBegin = itEnd;
      }

      for (size_t i = 0; i < nThreads; ++i) {
	wThreads[i].join();
      }

      delete [] wThreads;

      //Qprime has the next level of Q
      Q.swap( Qprime );
      ++level;
    }

    for (size_t i = 0; i < minSol.size(); ++i) {
      if (minSol[i]) {
	args.S.push_back( edges[i] );
      }
    }
  }
  // void single_run() {
  //   args.logg << "Beginning branch and bound procedure..." << endL;
  //   size_t level = 0;
  //   queue< solNode > Q;
  //   tinyGraph& g = args.g;
  //   solNode start;
  //   Q.push( start );
  //   while (!Q.empty()) {
  //     solNode curr = Q.front();
  //     if (curr.size >= minSize) {
  // 	//No need to consider this branch
  // 	Q.pop();
  // 	continue;
  //     }

  //     if (curr.level > level) {
  // 	level = curr.level;
  // 	args.logg << "Level: " << level << ", active solnodes: " << Q.size() << endL;
  //     }
      
  //     //update graph to remove S
  //     removeS( curr.partialSol );

  //     //If we are a left child, we need to see if we have
  //     //a feasible branch.
  //     if (curr.left_child) {

  // 	if (!(feasibleBelow( curr.partialSol ) ) ) {
  // 	  //Prune this branch, no feasible solutions below
  // 	  addS( curr.partialSol );
  // 	  Q.pop();
  // 	  continue;
  // 	}
  //     }
     
  //     //See if the partial solution is feasible.
  //     //If it is, no need to continue down this branch
  //     if (detectViol( false ) == dij.end()) {
  // 	//Solution is feasible
  // 	minSize = curr.size;
  // 	minSol = curr.partialSol;
	
  // 	addS( curr.partialSol );
  // 	Q.pop();
  // 	continue;
  //     }

  //     //Further work is needed on this branch
  //     //Split
  //     //Left child
  //     solNode lc( curr.partialSol, curr.level, true, curr.size );
  //     Q.push( lc );
  //     //Right child
  //     solNode rc( curr.partialSol, curr.level, false, curr.size );
  //     Q.push( rc );
      
  //     addS( curr.partialSol );
  //     Q.pop();
  //   }

  //   for (size_t i = 0; i < minSol.size(); ++i) {
  //     if (minSol[i]) {
  // 	args.S.push_back( edges[i] );
  //     }
  //   }
  // }
};

#endif
