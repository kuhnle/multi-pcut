## Scalable Algorithms for Length-Bounded Multicut Problem

#### Alan Kuhnle

The Length-Bounded Multicut Problem (LB MULTICUT): given a weighted network G,
set of vertex pairs S, and threshold T, determine a minimum-size set
of edges to remove from G such that the weighted shortest-path distance between
each pair in S is at least T.

This is the implementation of the algorithms SAP, MIA, and TAG,
to solve the LB MULTICUT problem as described in the following publication.

Alan Kuhnle, Victoria Crawford, and My T. Thai.
Network Resilience and the Length-Bounded Multicut
Problem: Reaching the Dynamic Billion-Scale with
Guarantees. Proceedings of the ACM on Measurement and
Analysis of Computing Systems, Vol. 2, No. 1, Article 4. Publication date:
March 2018.

### Dependencies 
* IBM CPLEX (can obtain free license for academic use, tested with version 12.71)
  https://www.ibm.com/products/ilog-cplex-optimization-studio
  (This is only required for MIA and OPT)
* The igraph library
  http://igraph.org/c/  
  Installing igraph on a Debian-based system should require only the following command:
```
   sudo apt-get install libigraph-dev
```
* The GNU g++ compiler, (tested with version 4.9.2)

### Provided programs
* `pip`: for running the algorithms on static graphs
* `pipDyn`: for running the dynamic version of TAG
* `preproc`: for preprocessing graphs into the correct format

Each of these is a target in the provided Makefile. The CPLEX variables in
the Makefile will need to be set to match your installation of CPLEX.
  
### Format of input graph

The programs take as input a custom binary format; the program
`preproc` is provided to convert an undirected edge list format
to the binary format. The first line of the edge list must be of the form
```
<Number of Nodes> <1 if weighted, 0 otherwise>
```
Each successive line describes an edge
```
<From id> <To id> <Weight (only if weighted)>
```
The node ids must be nonnegative integers; they are not restricted to lie in any range.
The `Weight` must be an unsigned integer.

If `graph.el` is an edge list in the above format, then
```
preproc graph.el graph.bin
```
generates the `graph.bin` file in the correct binary format for input to the other programs.

### Pair specification file
In addition to a graph in binary format, the programs take
a pair file as input, which specifies the set of pairs in
the input to the LB MULTICUT problem.

An example pair file is provided in `pairs.txt`; the first line
contains the number of pairs and each successive line contains the
two node ids comprising a single pair. (These ids must be within 0 to n - 1, where n is the number of nodes
in the network).

### `pip` usage
#### Options
```
-G <graph filename in binary format>
-p <target pair filename>
-T <threshold value>
-x <max number of threads (default 1)>
-D [run TAG]
-O [run OPT]
-P [run PRIMAL-DUAL]
-S [run SAP]
-E [run GEN]
-M [run MIA]
```
#### Example
To preprocess the provided edge list `graph.el`
```
preproc graph.el graph.bin
```
To run SAP on the included test graph with pairs in `pairs.txt` with threshold 4:
```
pip -G graph.bin -p pairs.txt -T 4 -S
```
To run MIA on the included test graph with pairs in `pairs.txt` with threshold 4:
```
pip -G graph.bin -p pairs.txt -T 4 -M
```
To run TAG on the included test graph with pairs in `pairs.txt` with threshold 4:
```
pip -G graph.bin -p pairs.txt -T 4 -D
```

### `pipDyn` usage
#### Options
```
-G <graph filename in binary format>
-p <target pair filename>
-T <threshold value>
-x <max number of threads (default 1)>
-A [Add <N> edges]
-R [Remove <N> edges]
-N <number of edges> (default: 10^6)
```
#### Example
To run dynamic TAG on the included test graph with pairs in `pairs.txt` with threshold 4, and 10 randomly inserted edges:
```
pipDyn -G graph.bin -p pairs.txt -T 4 -A -N 10
```