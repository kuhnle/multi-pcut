#include "mygraph.cpp"
#include "algs.cpp"
#include "proc_size.cpp"

#include <iostream>
#include <string>
#include <unistd.h>
#include <chrono>

using namespace mygraph;
using namespace std;

void print_help() {
  cout << "Options: " << endl;
  cout << "-G <graph filename in binary format>" << endl
       << "-p <target pair filename>" << endl
       << "-T <threshold value>" << endl
       << "-x <max number of threads (default 1)>" << endl
       << "-D [run TAG]" << endl
       << "-O [run OPT]" << endl
       << "-P [run PRIMAL-DUAL]" << endl
       << "-S [run SAP]" << endl
       << "-E [run GEN]" << endl
       << "-M [run MIA]" << endl;
}

void parseArgs( int argc, char** argv, Args& arg ) {
  int c;
  extern char *optarg;

  if (argc == 1) {
    print_help();
    exit( 2 );
  }

  string sarg;
  
  while ((c = getopt( argc, argv, ":G:DOT:p:vx:PSEBMNo:L") ) != -1) {
    switch(c) {
    case 'o':
       sarg.assign( optarg );
       arg.outputFileName = sarg;
       break;
    case 'L':
       arg.alg = PARL;
       break;
    case 'N':
       arg.alg = MIAE;
       arg.bMiaOpt = true;
       break;
    case 'M':
       arg.alg = MIA;
       break;
    case 'B':
      arg.alg = BNB;
      break;
    case 'E':
      arg.alg = GEN;
      break;
    case 'S':
      arg.alg = GSAMP;
      break;
    case 'P':
      arg.alg = PRIMALDUAL;
      break;
    case 'x':
      sarg.assign( optarg );
      arg.nThreads = stoi( sarg );
      break;
    case 'v':
      arg.bVerbose = true;
      break;
    case 'D':
      arg.alg = DARP;
      break;
    case 'O':
      arg.alg = OPT;
      break;
    case 'G':
      //graph specification
      arg.graphFileName.assign( optarg );
      break;
    case 'p':
      //target specification
      arg.pairFileName.assign( optarg );
      break;
    case 's':
      sarg.assign( optarg );
      //      arg.s = stoi( sarg );
      break;
    case 't':
      sarg.assign( optarg );
      //      arg.t = stoi( sarg );
      break;
    case 'T':
      sarg.assign( optarg );
      arg.T = stoi( sarg );
      break;
    case '?':
      print_help();
      exit( 2 );
      break;
    }
  }
}

void readGraph( Args& args ) {
   args.logg( INFO, "Reading graph from file: " + args.graphFileName + "..." );
   args.g.read_bin( args.graphFileName );
   args.logg( INFO, "Reading pairs from file: " + args.pairFileName + "..." );
   readPairs( args );
   args.logg( INFO, "Input finished.");
}

void runAlg( Args& args ) {
  clock_t t_start = clock();
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  switch (args.alg) {
  case DARP:
    {
      args.logg(INFO, "Starting DARP..." );
      Darp darp( args );
      darp.run();
    }
    break;

  case OPT:
    {
      args.logg(INFO, "Starting OPT..." );
      Opt opt( args );
      opt.run();
    }
    break;

  case PARL:
     {
	args.logg(INFO, "Starting PARL..." );
	Parl parl( args );
	parl.run();
     }
     break;
  case PRIMALDUAL:
    {
      args.logg(INFO, "Starting PRIMALDUAL..." );
      Darp pd( args );
      pd.run();
    }
    break;
    
  case GSAMP:
    {
      args.logg(INFO, "Starting GSAMP..." );
      Gsamp gsamp( args );
      gsamp.run();
    }
    break;

  case GEN:
    {
      args.logg(INFO, "Starting GEN..." );
      Gen gen( args );
      gen.run();
    }
    break;

  case BNB:
    {
      args.logg(INFO, "Starting BNB..." );
      BnB bnb( args );
      bnb.run();
    }
    break;

  case MIA:
    {
       //args.logg(INFO, "Starting ..." );
      Mia mia( args );
      mia.run();
    }
    break;
  case MIAE:
    {
       //args.logg(INFO, "Starting ..." );
      Mia mia( args );
      mia.run();
    }
    break;
   
  }

  args.tElapsed = elapsedTime( t_start );
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  size_t WallTimeMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
  args.wallTime = WallTimeMillis / 1000.0; 
}

void removeS( Args& args ) {
  vector < fEdge > removedEdges;
  for (size_t i = 0; i < args.S.size(); ++i) {
    if (!ctnsEdge( removedEdges, args.S[i] )) {
      removedEdges.push_back( args.S[i] );
      if ( args.g.remove_edge( args.S[i].x , args.S[i].y ) == 0) {
	args.logg << ERROR << "S contains an edge not present in the graph:";
	args.logg << args.S[i].x << " " << args.S[i].y << endL;
	args.logg << INFO;
      }
    } else {
      args.logg << ERROR << "S contains duplicate edge:";
      args.logg << args.S[i].x << " " << args.S[i].y << endL;
      args.logg << INFO;
    }
  }
}


void testFeasibility( Args& args ) {
  args.logg << "Starting feasibility test..." << endL;
  args.logg << "Rereading graph..." << endL;

  removePruned( args ); //Removes W from S
  
  readGraph( args );
  removeS ( args );
  
  args.bFeasible = true;
  for (size_t i = 0; i < args.nPairs; ++i ) {
      Dijkstra dij( args.P[i].x , args.P[i].y, args.g );
      uint32_t dst = dij.compute_dij();
      if (args.bVerbose)
	cout << "d( " << args.P[i].x << " , " << args.P[i].y << " ): " << dst << ", T: " << args.T << endl;
      if (dst <= args.T) {
	args.bFeasible = false;
	break;
      }
   }

   if (!args.bFeasible) {
     args.logg << ERROR << "Solution is infeasible." << endL << INFO;
   } else {
     args.logg << INFO << "Solution has been verified to be feasible." << endL;
   }
}




void outputResults( Args& args ) {
  args.logg << INFO;
  args.logg << "Size W: " << args.W.size() << endL;
  args.logg << "Size S: " << args.S.size() << endL;
  if (args.bVerbose) {
    print_vector( args.S, cout );
  }
  args.logg << "Elapsed CPU time(s): " << args.tElapsed << endL;
  args.logg << "Elapsed WALL time(s): " << args.wallTime << endL;

  if (!args.outputFileName.empty()) {
     args.maxMem = getPeakRSS() / (1024.0 * 1024.0);
     ofstream ofile ( args.outputFileName.c_str(), ofstream::out | ofstream::app );
     ofile << args.graphFileName << ' ' << args.alg << ' ' << args.T << ' ' << args.nPairs << ' '
	   << args.S.size() << ' ' << args.tElapsed << ' ' << args.wallTime << ' ' << args.maxMem << ' ' << args.miaCuts << ' ' << args.nThreads << endl;
     ofile.close();
  }
}

int main(int argc, char** argv) {
  Args args;
  parseArgs( argc, argv, args );
  readGraph( args );
  runAlg( args );
  if (args.outputFileName.empty()) {
     testFeasibility( args );
  } else {
     args.logg << "Skipping feasibility check..." << endL;
     removePruned( args ); //Removes W from S
  }
  outputResults( args );
}
