CPPFLAGS=-std=c++11 -Wall -O3
CPLEX_INCLUDE=-I/opt/ibm/ILOG/CPLEX_Studio1271/cplex/include -I/opt/ibm/ILOG/CPLEX_Studio1271/concert/include
CPLEX_LIB=-L/opt/ibm/ILOG/CPLEX_Studio1271/cplex/lib/x86-64_linux/static_pic -L/opt/ibm/ILOG/CPLEX_Studio1271/concert/lib/x86-64_linux/static_pic -lilocplex -lcplex -lconcert -lm -lpthread
CPLEX_FLAGS=${CPLEX_INCLUDE} ${CPLEX_LIB} -DNDEBUG -DILOSTRICTPOD -DIL_STD

pip: main.cpp mygraph.cpp algs.cpp
	g++ main.cpp -o pip `pkg-config --cflags --libs igraph`	${CPPFLAGS} ${CPLEX_FLAGS} -ligraph
preproc: preprocess.cpp mygraph.cpp
	g++ preprocess.cpp -o preproc  ${CPPFLAGS}
pipDyn: *.cpp
	g++ pip_dyn.cpp -o pipDyn `pkg-config --cflags --libs igraph` -std=c++11 -O3  ${CPLEX_FLAGS} -ligraph

